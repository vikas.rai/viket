class Matches {
    constructor() {
        this.renderMatchCards();
        let _obj = this;
        this.scoreUpdateInterval = setInterval(function () { _obj.updateScore(); }, 10000);
    }
    destory() {
        clearInterval(this.scoreUpdateInterval);
    }
    createMatchCards(matches, isScore) {
        let MatchCardHtml = "";
        matches.forEach(element => {
            if (element.status == "Not covered Live") {
                console.log(element.status);
                console.log('----------------------')
            }
            else {
                try {
                    let Html = `
                    <div class="col-lg-4 col-sm-6 col-12 match-card" data-match-id="${element.objectId}">
                    <a href="/#MatchDetails?SeriesID=${element.series.objectId}&MatchID=${element.objectId}">
                        <div class="bg-white p-1 card-container">
                            <div class="d-flex">
                                <span class="title">${element.title || ""}</span>
                                <span class="dot">&nbsp;•&nbsp;</span>
                                <span class="format">${element.format}</span>
                                <span class="dot">&nbsp;•&nbsp;</span>
                                <span class="series-name">${element.series.name}</span>
                            </div>
                            <div class="d-inline-block team-details">
                                <span>
                                    <img class="match-card-logo"
                                    src="${Common.getTeamImageURL(element.teams[0].team.image)}" />
                                    </span>
                                <span class="team1-name mx-2">${element.teams[0].team.longName}</span>
                                ${element.teams[0].score == null ? "" : "<span class='team1-score mx-2 float-end'>" + (element.teams[0].scoreInfo == null ? "" : "(" + element.teams[0].scoreInfo + ") ") + element.teams[0].score + "</span>"}
                            </div>
                            <div class="d-inline-block team-details">
                                <span>
                                    <img class="match-card-logo"
                                        src="${Common.getTeamImageURL(element.teams[1].team.image)}" />
                                </span>
                                <span class="team2-name mx-2">${element.teams[1].team.longName}</span>
                                ${element.teams[1].score == null ? "" : "<span class='team1-score mx-2 float-end'>" + (element.teams[1].scoreInfo == null ? "" : "(" + element.teams[1].scoreInfo + ") ") + element.teams[1].score + "</span>"}
                            </div>
                            <div class="d-flex">
                            <span style="color:#e08989">Venue- ${element.ground.longName}, ${moment(element.startTime).format('MMMM DD, yyyy')}</span>
                            </div>
                            <div class="d-flex">
                            ${element.state != "PRE" ? "<span class='text-danger text-bold me-2'>" + element.status + "</span>" : ""}
                            ${element.state == "PRE" ? "<span class='date-time'>Starts at " + moment(element.startTime).format('h:mm A') + "</span>" : ""}
                            ${element.statusText != null && !element.statusText.includes('Match starts') ? "<span class='mx-2 text-primary'> " + element.statusText + "</span>" : ""}
                            </div>
                        </div>
                    </a>
                </div> `;
                    let matchCard = document.querySelector(`.match-card-container .match-card[data-match-id="${element.objectId}"]`)
                    if (isScore && matchCard != null) {
                        matchCard.outerHTML = Html;
                    }
                    else if (!isScore && matchCard == null) {
                        MatchCardHtml += Html;
                    }
                } catch (error) {
                    console.log(error);
                    console.log(element);
                    console.log('----------------------')
                }
            }
        });
        return MatchCardHtml
    }
    async updateScore() {
        let matches = await HttpHelper.Get(getApiURL("Matches/Current"));
        //let matches = await HttpHelper.Get(getApiURL(`Matches/ScoreCard?SeriesID=1298423&MatchID=1304076`));

        let container = document.getElementsByClassName('match-card-container');
        this.createMatchCards(matches, true);
    }
    async renderMatchCards() {
        let matches = await HttpHelper.Get(getApiURL("Matches/Trending"));
        //let matches = await HttpHelper.Get(getApiURL(`Matches/ScoreCard?SeriesID=1298423&MatchID=1304076`));
        let container = document.getElementsByClassName('match-card-container');
        let html = this.createMatchCards(matches);
        container[0].innerHTML += html;
        this.updateScore();
    }
}
