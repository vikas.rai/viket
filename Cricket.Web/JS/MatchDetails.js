class MatchDetails {
    constructor() {
        this.Tabs = [
            { DisaplyName: "Scorecard", Name: "scorecard", IsActive: false, Order: 2, isDisplay: true },
            { DisaplyName: "Commentary", Name: "commentary", IsActive: false, Order: 3, isDisplay: true },
            { DisaplyName: "Preview", Name: "preview", IsActive: false, Order: 1, isDisplay: true },
            { DisaplyName: "Fantasy XI", Name: "fantasy", IsActive: false, Order: 4, isDisplay: true }]
        this.scoreUpdateInterval = null
        this.tabUpdateInterval = null
        this.PreviewData = null
        this.fantasyData = null
        this.match = {}
        this.init();
    }
    async init() {
        await this.renderMatchDetails();
        this.renderMatchDetailsTab();

        let _obj = this;
        this.scoreUpdateInterval = setInterval(function () { _obj.renderMatchDetails(); }, 10000);

    }
    destory() {
        clearInterval(this.scoreUpdateInterval);
    }
    InningHtml(inning) {
        let html = '<div class="pt-1 live-score-container"><table class="table live-score tbl-batsmen">';
        html += `
            <tr>
                <td><span>Batters</span></td>
                <td><span>R(B)</span></td>
                <td><span>4s</span></td>
                <td><span>6s</span></td>
                <td><span>SR</span></td>
            </tr>`;

        let batted = inning.inningBatsmen.filter(function (item) {
            return item.battedType == 'yes'
        })

        let notBatted = inning.inningBatsmen.filter(function (item) {
            return item.battedType != 'yes'
        })

        let notBatedNames = notBatted.map(function (item) {
            return item.player.longName;
        })

        let Fow = inning.inningBatsmen.filter(function (item) {
            return item.fowRuns != null
        })

        batted.forEach(element => {
            html += `
                <tr>
                    <td data-id=${element.player.objectId}><span>${element.player.longName} ${element.currentType == 1 ? "<span class='text-success' style='font-weight:900'>*<span>" : ""}</span><br><span class="text-danger dismissal">${element.dismissalText.long}</span></td>
                    <td><span>${element.runs || 0}(${element.balls || 0})</span></td>
                    <td><span>${element.fours || 0}</span></td>
                    <td><span>${element.sixes || 0}</span></td>
                    <td><span>${element.strikerate || 0}</span></td>
                </tr>`
        });

        html += `
        <tr>
            <td><span>Extras</span></td>
            <td><span>${inning.extras}</span></td>
            <td colspan="3" class="text-end"><span>(LB ${inning.legbyes}, NB ${inning.noballs}, W ${inning.wides})</span></td>
        </tr>`;

        html += '</table>' + (notBatted.length > 0 ? '<div class="p-1" style="line-height: 1;"><span>Did not bat: </span><span style="font-weight: unset;">' + notBatedNames.join(', ') + '</span></div>' : '') + '</div>';

        let fowHtml = ''
        Fow.forEach((element, index) => {
            fowHtml += `${index + 1}-${element.fowRuns}(${element.player.longName}, ${element.fowOvers} ov)`
            if (index < Fow.length - 1)
                fowHtml += ', ';
        });

        html += '<div class="p-1" style="line-height: 1;"><span>Fall of wickets: </span><span style="font-weight: unset;">' + fowHtml + '</span></div>'

        /// Bowler
        html += '<div class="pt-1 live-score-container"><table class="table live-score tbl-bowler">';
        html += `
            <tr>
                <td><span>Bowlers</span></td>
                <td><span>O(R)</span></td>
                <td><span>M</span></td>
                <td><span>W</span></td>
                <td><span>ECON</span></td>
                <td><span>WD</span></td>
                <td><span>NB</span></td>
            </tr>
        `;
        inning.inningBowlers.forEach(element => {
            html += `
                <tr>
                    <td data-id=${element.player.objectId}><span>${element.player.longName}</span></td>
                    <td><span>${element.overs || 0}(${element.conceded || 0})</span></td>
                    <td><span>${element.maidens || 0}</span></td>
                    <td><span>${element.wickets || 0}</span></td>
                    <td><span>${element.economy || 0}</span></td>
                    <td><span>${element.wides || 0}</span></td>
                    <td><span>${element.noballs || 0}</span></td>
                </tr>`
        });
        html += '</table></div>';

        return html;

    }
    async ScorCardHtml(isUpdate) {
        let SeriesID = VikasJS.getQueryString("SeriesID");
        let MatchID = VikasJS.getQueryString("MatchID");
        let data = await HttpHelper.Get(getApiURL(`Matches/ScoreCard?SeriesID=${SeriesID}&MatchID=${MatchID}`));

        let html = '<div class="accordion" id="accordion-scorecard">';
        for (let index = 0; index < data.content.scorecard.innings.length; index++) {
            const element = data.content.scorecard.innings[index];
            if (isUpdate) {
                let el = document.querySelector('#collapse' + element.inningNumber + ' .accordion-body')
                el.innerHTML = this.InningHtml(element)
                return;
            }
            html += `
                <div class="accordion-item ${index ? "mt-2" : ""}">
                    <h2 class="accordion-header" id="innings-${element.inningNumber}">
                        <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapse${element.inningNumber}" aria-expanded="true" aria-controls="collapseOne">
                            ${element.team.longName} Innings
                        </button>
                    </h2>
                    <div id="collapse${element.inningNumber}" class="accordion-collapse collapse show" aria-labelledby="innings-${element.inningNumber}" data-bs-parent="#accordion-scorecard">
                        <div class="accordion-body">
                        ${this.InningHtml(element)}

                        </div>
                    </div>
                </div>
            `;
        }
        html += '</div>'
        return html;
    }
    async CommentaryHtml() {

    }
    async PreviewHtml() {
        let SeriesID = VikasJS.getQueryString("SeriesID");
        let MatchID = VikasJS.getQueryString("MatchID");
        if (this.PreviewData == null) {
            this.PreviewData = await HttpHelper.Get(getApiURL(`Matches/Preview?SeriesID=${SeriesID}&MatchID=${MatchID}`))
                .then(function (response) {
                    let data = response.prview;
                    let html = `
                    <h2>${data.story.title}</h2>
                    <p>${data.story.summary}</p>
                    `;
                    data.content.items.forEach(element => {
                        if (element.type == 'HTML') {
                            html += `<p>${element.html}</p>`;
                        }
                    });
                    let el = document.createElement("div");
                    el.innerHTML = html;
                    let a = el.querySelectorAll('a');
                    a.forEach(element => {
                        element.removeAttribute("href");
                    });

                    let players = ''
                    if (response.squad) {
                        response.squad.forEach(element => {
                            let pHtml = '';
                            let playingXI = element.players.map(function (item) {
                                return item.player.longName
                            })
                            players += '<p><b>' + element.team.longName + ' Squads : </b>' + playingXI.join(', ') + '</p>'
                        });
                    }
                    return el.innerHTML + players;
                }).catch(function (e) {
                    console.log("Matches/Preview-" + e.message)
                    console.log(e.stack)
                    return "Preview coming soon..."
                });
        }
        return this.PreviewData;
    }
    async FantasyHtml() {
        let SeriesID = VikasJS.getQueryString("SeriesID");
        let MatchID = VikasJS.getQueryString("MatchID");
        if (this.fantasyData == null) {
            this.fantasyData = await HttpHelper.Get(getApiURL(`Matches/Fantasy?SeriesID=${SeriesID}&MatchID=${MatchID}`))
                .then(function (response) {
                    let data = response.fantasy;
                    let html = `
                    <h2>${data.story.title}</h2>`;
                    var i = 0;
                    data.content.items.forEach(element => {
                        if (element.type == 'HTML') {
                            if (i == 0) {
                                i++;
                                return;
                            }
                            html += `<p>${element.html}</p>`;
                        }
                    });
                    let el = document.createElement("div");
                    el.innerHTML = html;
                    let a = el.querySelectorAll('a');
                    a.forEach(element => {
                        element.removeAttribute("href");
                    });

                    let players = ''
                    if (response.squad) {
                        response.squad.forEach((element, i) => {
                            let playingXI = element.players.map(function (item) {
                                return item.player.longName
                            })
                            players += '<p><b>' + element.team.longName + ' Squads : </b>' + playingXI.join(', ') + '</p>'
                        });
                    }
                    return el.innerHTML + players;
                }).catch(function (e) {
                    console.log("Matches/Fantasy-" + e.message)
                    console.log(e.stack)
                    return "Fantasy XI coming soon..."
                });
        }
        return this.fantasyData;
    }
    async TabContentHtml(tab) {
        clearInterval(this.tabUpdateInterval);
        let html = '';
        if (tab == 'scorecard') {
            html = await this.ScorCardHtml() + await this.matchInfo();
            this.tabUpdateInterval = setInterval(() => {
                this.ScorCardHtml(true);
            }, 30000);
        }
        else if (tab == 'preview') {
            html = await this.PreviewHtml();
        }
        else if (tab == 'fantasy') {
            html = await this.FantasyHtml();
        }
        else {
            html = tab + 'tab'
        }
        return html;
    }
    async matchInfo() {
        let SeriesID = VikasJS.getQueryString("SeriesID");
        let MatchID = VikasJS.getQueryString("MatchID");
        let tossWinnerTeamId = this.match.tossWinnerTeamId;
        let tossWinnerChoice = this.match.tossWinnerChoice == 1 ? "opted to bat first" : "opted to field first"
        let tossWinnerteam = this.match.teams.filter(function (item) {
            return item.team.id == tossWinnerTeamId
        })
        let data = await HttpHelper.Get(getApiURL(`Matches/PlayingXI?SeriesID=${SeriesID}&MatchID=${MatchID}`))

        let html = '';
        let players = []
        if (data.playingXI && data.playingXI.teamPlayers) {
            data.playingXI.teamPlayers.forEach(element => {
                let pHtml = '';
                let playingXI = element.players.map(function (item) {
                    return item.player.longName
                })
                players[element.team.id] = { html: '<div class="p-1" style="line-height: 1;"><span>' + element.team.longName + ' Playing : </span><span style="font-weight: unset;">' + playingXI.join(', ') + '</span></div>', Name: element.team.longName }

            });
        }
        let benchPlayers = []
        if (data.bench) {
            Object.keys(data.bench).forEach(element => {
                let pHtml = '';
                let bench = data.bench[element].map(function (item) {
                    return item.player.longName
                })
                benchPlayers[element] = '<div class="p-1" style="line-height: 1;"><span>' + players[element].Name + ' Bench : </span><span style="font-weight: unset;">' + bench.join(', ') + '</span></div>'

            });
        }
        Object.keys(data.bench).forEach(element => {
            html += '<div class="mt-2"><table class="table match-info"><tr><td colspan="2">';
            html += players[element].html;
            html += benchPlayers[element];
            html += '</td></tr></table></div>'
        });

        html += '<div class="mt-2"><table class="table match-info">';
        html += `
            <tr>
                <td colspan="2"><span class="text-success">Match Info</span></td>
            </tr>
        `;
        if (tossWinnerteam && tossWinnerteam.length > 0) {
            html += `
                <tr>
                    <td><span>Toss</span></td>
                    <td>
                        <div style="line-height: 0.7;padding: 7px 0;">
                            <img class="match-card-logo"
                            src="${Common.getTeamImageURL(tossWinnerteam[0].team.image)}" />
                            <span>${tossWinnerteam[0].team.longName} won the toss and ${tossWinnerChoice}</span>
                        </div>
                    </td>
                </tr>`
        }
        if (this.match.umpires && this.match.umpires.length > 0) {
            let umpires = this.match.umpires.map(function (item) {
                return `<div>
                <img class="match-card-logo"
                src="${Common.getTeamImageURL(item.team.image)}" />
                <span>${item.player.longName} (${item.team.longName})</span>
                </div>`
            })
            html += `
                <tr>
                    <td><span>Umpires</span></td>
                    <td><span>${umpires.join('')}</span></td>
                </tr>`
        }
        if (this.match.tvUmpires && this.match.tvUmpires.length > 0) {

            let tvUmpires = this.match.tvUmpires.map(function (item) {
                return `<div>
                <img class="match-card-logo"
                src="${Common.getTeamImageURL(item.team.image)}" />
                <span>${item.player.longName} (${item.team.longName})</span>
                </div>`
            })
            html += `
                <tr>
                    <td><span>TV Umpire</span></td>
                    <td><span>${tvUmpires.join('')}</span></td>
                </tr>`
        }
        if (this.match.reserveUmpires && this.match.reserveUmpires.length > 0) {
            let reserveUmpires = this.match.reserveUmpires.map(function (item) {
                return `<div>
                <img class="match-card-logo"
                src="${Common.getTeamImageURL(item.team.image)}" />
                <span>${item.player.longName} (${item.team.longName})</span>
                </div>`
            })
            html += `
                <tr>
                    <td><span>Reserve Umpire</span></td>
                    <td><span>${reserveUmpires.join('')}</span></td>
                </tr>`
        }
        if (this.match.matchReferees && this.match.matchReferees.length > 0) {
            let matchReferees = this.match.matchReferees.map(function (item) {
                return `<div>
                <img class="match-card-logo"
                src="${Common.getTeamImageURL(item.team.image)}" />
                <span>${item.player.longName} (${item.team.longName})</span>
                </div>`
            })
            html += `
                <tr>
                    <td><span>Match Referee</span></td>
                    <td><span>${matchReferees.join('')}</span></td>
                </tr>`;
        }

        html += '</table></div>'
        return html
    }
    async tabClick(btn) {
        let tabName = btn.attributes['aria-controls'].value;
        let tabContent = document.querySelector('#' + tabName)
        tabContent.innerHTML = '<div class="text-center"><i class="fa fa-spinner fa-spin fa-3x"></i></div>'

        let html = await this.TabContentHtml(tabName);
        tabContent.innerHTML = html;
    }
    async createTabsHtml() {
        let htmlUl = '<ul class="nav nav-tabs" id="match-tab" role="tablist">';
        let htmlTab = '<div class="tab-content" id="match-tab-content">';
        let tabs = this.Tabs.sort((a, b) => parseFloat(a.Order) - parseFloat(b.Order));
        tabs = tabs.map(function (item) {
            if (app.currentPageClass.match.state == 'PRE' && (item.Name == 'scorecard' || item.Name == 'commentary')) {
                item.isDisplay = false;
            }
            else if (app.currentPageClass.match.state != 'PRE' && item.Name == 'preview') {
                item.isDisplay = false;
            }
            return item;
        });
        for (let index = 0; index < tabs.length; index++) {
            const element = tabs[index];
            if (!element.isDisplay) {
                continue;
                //if (this.match.state == 'PRE') {
            }
            if (htmlUl == '<ul class="nav nav-tabs" id="match-tab" role="tablist">') {
                element.IsActive = true
            }
            htmlUl += `
                    <li class="nav-item" role="presentation">
                        <button onclick="app.currentPageClass.tabClick(this)" class="nav-link ${element.IsActive ? 'active' : ''}" id="${element.Name}-tab" data-bs-toggle="tab"
                            data-bs-target="#${element.Name}" type="button" role="tab" aria-controls="${element.Name}"
                            aria-selected="true">${element.DisaplyName}</button>
                    </li>`

            htmlTab += `                
                    <div class="tab-pane fade show ${element.IsActive ? 'active' : ''}" id="${element.Name}" role="tabpanel"
                        aria-labelledby="${element.Name}-tab">
                        $h{html}
                    </div>`
        }

        htmlUl += "</ul>";
        htmlTab += "</div>";

        return { htmlTab, htmlUl }
    }

    createBattingHtml(batsmen, isUpdate) {
        let html = `
            <tr>
                <td><span>Batters</span></td>
                <td><span>R(B)</span></td>
                <td><span>4s</span></td>
                <td><span>6s</span></td>
                <td><span>SR</span></td>
            </tr>
        `;
        batsmen.forEach(element => {
            html += `
            <tr>
                <td data-id=${element.player.objectId}><span>${element.player.longName} ${element.currentType == 1 ? "<span class='text-success' style='font-weight:900'>*<span>" : ""}</span></td>
                <td><span>${element.runs || 0}(${element.balls || 0})</span></td>
                <td><span>${element.fours || 0}</span></td>
                <td><span>${element.sixes || 0}</span></td>
                <td><span>${element.strikerate || 0}</span></td>
            </tr>`
        });

        if (isUpdate) {
            let tblBatsmen = document.getElementsByClassName("tbl-batsmen")[0];
            tblBatsmen.innerHTML = html;
            tblBatsmen.innerHTML = html;

        }

        return html;
    }

    createBowlingHtml(bowlers, isUpdate) {
        let html = `
            <tr>
                <td><span>Bowlers</span></td>
                <td><span>O(R)</span></td>
                <td><span>M</span></td>
                <td><span>W</span></td>
                <td><span>ECON</span></td>
            </tr>`;
        bowlers.forEach(element => {
            html += `
            <tr>
                <td data-id=${element.player.objectId}><span>${element.player.longName}</span></td>
                <td><span>${element.overs || 0}(${element.conceded || 0})</span></td>
                <td><span>${element.maidens || 0}</span></td>
                <td><span>${element.wickets || 0}</span></td>
                <td><span>${element.economy || 0}</span></td>
            </tr>`
        });

        if (isUpdate) {
            let tblBowlers = document.getElementsByClassName("tbl-bowlers")[0];
            tblBowlers.innerHTML = html;
        }

        return html;
    }

    createMatchDetails(data) {
        let match = data.match
        let liveInfo = data.content.supportInfo.liveInfo;
        let batsmenHtml = data.content.supportInfo.liveSummary ? this.createBattingHtml(data.content.supportInfo.liveSummary.batsmen) : "";
        let bowlersHtml = data.content.supportInfo.liveSummary ? this.createBowlingHtml(data.content.supportInfo.liveSummary.bowlers) : "";
        let playersOfTheMatch = data.content.supportInfo.playersOfTheMatch;
        if (match.status == "Not covered Live") {
            console.log(match.status);
            console.log('----------------------')
        }
        else {
            let Html = `
                    <div data-match-id="${match.objectId}">
                        <div class="d-flex">
                            <span class="title">${match.title || ""}</span>
                            <span class="dot">&nbsp;•&nbsp;</span>
                            <span class="format">${match.format}</span>
                            <span class="dot">&nbsp;•&nbsp;</span>
                            <span class="series-name">${match.series.name}</span>
                        </div>
                        <div class="d-flex">
                            <span style="color:#e08989">Venue- ${match.ground.longName}, ${moment(match.startTime).format('MMMM DD, yyyy')}</span>
                        </div>
                        <div class="d-inline-block team-details">
                            <span>
                                <img class="match-card-logo"
                                src="${Common.getTeamImageURL(match.teams[0].team.image)}" />
                                </span>
                            <span class="team1-name mx-2">${match.teams[0].team.longName}</span>
                            ${match.teams[0].score == null ? "" : "<span class='team1-score mx-2 float-end'>" + (match.teams[0].scoreInfo == null ? "" : "(" + match.teams[0].scoreInfo + ") ") + match.teams[0].score + "</span>"}
                        </div>
                        <div class="d-inline-block team-details">
                            <span>
                                <img class="match-card-logo"
                                    src="${Common.getTeamImageURL(match.teams[1].team.image)}" />
                            </span>
                            <span class="team2-name mx-2">${match.teams[1].team.longName}</span>
                            ${match.teams[1].score == null ? "" : "<span class='team1-score mx-2 float-end'>" + (match.teams[1].scoreInfo == null ? "" : "(" + match.teams[1].scoreInfo + ") ") + match.teams[1].score + "</span>"}
                        </div>
                        <div class="d-flex pt-1">
                            ${liveInfo ? '<span>Current RR-' + liveInfo.currentRunRate + (liveInfo.requiredRunrate ? ", Required RR- " + liveInfo.requiredRunrate : "") + '</span>' : ''}
                        </div>
                        <div class="d-flex">
                            ${match.state != "PRE" ? "<span class='text-danger text-bold me-2'>" + match.status + "</span>" : ""}
                            ${match.state == "PRE" ? "<span class='date-time'>Starts at " + moment(match.startTime).format('h:mm A') + "</span>" : ""}
                            ${match.statusText != null && !match.statusText.includes('Match starts') ? "<span class='mx-2 text-primary'> " + match.statusText + "</span>" : ""}
                        </div>
                        ${playersOfTheMatch && playersOfTheMatch[0] ? '<div class="d-flex"><span> Player Of The Match- <span><img class="mom-logo"src="' + Common.getTeamImageURL(playersOfTheMatch[0].player.image) + '" /></span> ' + playersOfTheMatch[0].player.longName + '</span></div>' : ''}
                        <div class="d-flex  pt-1 live-score-container">
                            <table class="table live-score tbl-batsmen">  
                                ${batsmenHtml}
                            </table>
                        </div>
                        <div class="d-flex live-score-container">
                            <table class="table live-score tbl-bowlers">
                                ${bowlersHtml}
                            </table>
                        </div>
                        </div>
                `;
            return Html;
        }
    }
    async updateMatchDetails() {
        let data = await this.getMatchDetailsData();
        this.createBattingHtml(data.content.supportInfo.liveSummary.batsmen, true);
        this.createBowlingHtml(data.content.supportInfo.liveSummary.bowlers, true);
    }

    async getMatchDetailsData() {
        let SeriesID = VikasJS.getQueryString("SeriesID");
        let MatchID = VikasJS.getQueryString("MatchID");
        //let SeriesID = 1297650;
        //let MatchID = 1297684;
        let data = await HttpHelper.Get(getApiURL(`Matches/LiveMatch?SeriesID=${SeriesID}&MatchID=${MatchID}`));
        return data;
    }

    async renderMatchDetails() {
        let data = await this.getMatchDetailsData();
        this.match = data.match;
        let container = document.querySelector('.match-details-container .match-details');
        let html = this.createMatchDetails(data);
        container.innerHTML = html;
    }
    async renderMatchDetailsTab() {
        let tabContainer = document.querySelector('.match-details-container .match-info .match-tab-container');
        let tabContent = document.querySelector('.match-details-container .match-info .match-tab-content');
        let html = await this.createTabsHtml();
        tabContainer.innerHTML += html.htmlUl;
        tabContent.innerHTML += html.htmlTab;
        let btnTab = document.querySelector('#match-tab .nav-link.active')
        this.tabClick(btnTab);
    }
}
