﻿//created by Vikas November 2017
class VikasModal {
    Alert(Message, OnClose, Type) {
        this.Templet = $(this.AlertTemplet()).attr('id', (new Date()).getTime()).clone();
        $('body').append(this.Templet);
        this.addEvent('.closeError', 'click', this.Close, OnClose)
        this.Templet.find('.modal-body').html(Message);
        if (Type == 'Success') {
            this.Templet.find('.modal-title').html('Success');
            this.Templet.find('.modal-header').css('background-color', '#19ff00');
            this.Templet.find('.modal-content').css('border', '5px solid rgb(25, 255, 0)');
        }
        else if (Type == 'Warning') {
            this.Templet.find('.modal-title').html('Warning');
            this.Templet.find('.modal-header').css('background-color', 'rgba(255, 138, 0, 0.93)');
            this.Templet.find('.modal-content').css('border', '5px solid rgb(255, 146, 18)');
        }
        else {
            this.Templet.find('.modal-title').html('Error');
        }
        if (Message.length < 70) {
            this.Templet.find('.modal-dialog').addClass('modal-sm');
        }
        this.Templet.modal({ backdrop: 'static' });
        this.Templet.find('.modal-dialog').draggable({
            handle: ".modal-header"
        });
        $('.modal-backdrop.in').css("opacity", "0.1");
        this.Templet.find(".modal-header").css({ "cursor": "default" });
    }
    Confirm(Message, OnOK, OnCancel) {
        this.Templet = $(this.ConfirmTemplet()).attr('id', (new Date()).getTime()).clone();
        this.addEvent('.closeError', 'click', this.Close, OnCancel)
        this.addEvent('.Yes', 'click', this.Close, OnOK)
        this.Templet.find('.modal-body').html(Message);
        if (Message.length < 70) {
            this.Templet.find('.modal-dialog').addClass('modal-sm');
        }
        this.Templet.modal({ backdrop: 'static' });
        this.Templet.find('.modal-dialog').draggable({
            handle: ".modal-header"
        });
        $('.modal-backdrop.in').css("opacity", "0.1");
        this.Templet.find(".modal-header").css({ "cursor": "default" });
    }
    PopUp(Content, Title) {
        this.Templet = $(this.PopUpTemplet()).attr('id', (new Date()).getTime()).clone();
        this.addEvent('.closeError', 'click', this.Close)
        this.Templet.find('.modal-body').html(Content);
        this.Templet.find('.modal-title').html(Title);
        this.Templet.modal({ backdrop: 'static' });
        this.Templet.find('.modal-dialog').draggable({
            handle: ".modal-header"
        });
        $('.modal-backdrop.in').css("opacity", "0.1");
        this.Templet.find(".modal-header").css({ "cursor": "default" });
    }
    AlertTemplet() {
        return ` <div class="modal fade" id="myModal" role="dialog" style="overflow: auto;">
        <div class="modal-dialog">
            <div class="modal-content" style="border-radius:0;border: 5px solid rgb(255, 84, 84);">
                <div class="modal-header" style="height: 30px; padding-top: 0px;background-color:#ff0000ab;color:white;">
                    <button type="button" class ="close closeError" style="font-size:xx-large;margin-top: -5px;margin-right: -12px;font-weight: 100;">&times; </button>
                    <h4 class="modal-title text-center">Error !</h4>
                </div>
                <div class="modal-body">
                    Some text in the modal.
                </div>
            <div class ="modal-footer"style="height: 50px;padding: 8px;margin-top: 0px;">
                    <button type="button" class ="btn btn-default closeError">Close</button>
                </div>
            </div>
        </div>
    </div>`;
    }
    ConfirmTemplet() {
        return ` <div class="modal fade" id="myModal" role="dialog" style="overflow: auto;">
        <div class="modal-dialog">
            <div class="modal-content" style="border-radius:0;border: 5px solid rgb(255, 84, 84);">
                <div class="modal-header" style="height: 30px; padding-top: 0px;background-color:#ff0000ab;color:white;">
                    <button type="button" class ="close closeError" style="font-size:xx-large;margin-top: -5px;margin-right: -12px;font-weight: 100;">&times; </button>
                    <h4 class="modal-title text-center">Confirm</h4>
                </div>
                <div class="modal-body">
                    Some text in the modal.
                </div> <div class="modal-footer"style="height: 50px;padding: 8px;margin-top: 0px;">
                    <button type="button" class ="btn btn-success Yes">Yes</button>
                    <button type="button" class ="btn btn-danger closeError">No</button>
                </div>
            </div>
        </div>
    </div>`;
    }
    PopUpTemplet() {
        return ` <div class="modal fade" id="myModal" role="dialog" style="overflow: auto;">
        <div class="modal-dialog">
            <div class ="modal-content" style="border-radius:0;border: 5px solid rgb(48, 167, 255);">
                <div class ="modal-header" style="height: 30px; padding-top: 0px;background-color:rgba(0, 147, 255, 0.81);color:white;">
                    <button type="button" class ="close closeError" style="font-size:xx-large;margin-top: -5px;margin-right: -12px;font-weight: 100;">&times; </button>
                    <h4 class="modal-title text-center">Modal !</h4>
                </div>
                <div class ="modal-body scroll" style="max-height:70vh;Overflow:auto;">
                    Some text in the modal.
                </div> <div class="modal-footer"style="height: 50px;padding: 8px;margin-top: 0px;">
                    <button type="button" class ="btn btn-default closeError">Close</button>
                </div>
            </div>
        </div>
    </div>`;
    }
    Close(Templet) {
        if (typeof Templet != 'undefined') {
            Templet.modal('hide');
            setTimeout(function () { Templet.remove(); }, 500);
        }
    }
    addEvent(Obj, Event, Fun, callback) {
        let Templet = this.Templet;
        if (typeof Obj != 'undefined' && typeof Event != 'undefined' && typeof Fun != 'undefined' && Event != '') {
            $('body').off(Event, '#' + Templet.attr('id') + ' ' + Obj);
            $('body').on(Event, '#' + Templet.attr('id') + ' ' + Obj, function (e) {
                Fun(Templet);
                if (typeof callback != 'undefined') {
                    callback();
                }
            });
        }
    }
}
class VikasLoader {
    static Templet(msg) {
        return `<div id="Loader" style="position: fixed;z-index: 10000; left: 0; top: 0; width: 100%; height: 100%; overflow: auto; background-color: rgba(0, 0, 0, 0.73);">
                        <div id="content" style="position:absolute;top:40%;background-color: #fefefe; margin: auto; padding: 20px; padding-left: 100px; padding-right: 100px; width: 100%; -webkit-box-shadow: -2px -2px 47px 6px rgb(209, 200, 209); -moz-box-shadow: -2px -2px 47px 6px rgb(209, 200, 209); box-shadow: -2px -2px 47px 6px rgb(209, 200, 209);">
                        <p ><i class="fa fa-refresh fa-spin fa-2x"></i><span style="padding-left: 10px; position: relative;font-size:30px;">` + msg + `</span></p>
                        </div>
                        </div>`;
    }
    static Show(msg) {
        $('body').append(VikasLoader.Templet(msg));
        if (parseInt($(window).width()) < 600) {
            $('#Loader').find('#content').css({ "padding-right": "0px", "padding-left": "50px" });
            $('#Loader').find('#content p span').css({ "font-size": "20px" });
        }
    }
    static Hide() {
        $('#Loader').remove();
    }
}
class VikasTimer {
    constructor(Obj, Option) {
        this.Obj = null;
        this.Flag = false;
        this.Pause = null;
        this.Resume = null;
        this.Timer = null;
        this.RepeatTimer = false;
        this.TimeUp = null;
        this.DisplayTime = $('<span >00:00:00</span>');
        this.timerStart = null;
        this.TimerStopTime = -1;
        this.Option = Option;
        this.totalSec = 0;
        if (Obj.length == 0)
            return alert('Count down parameter invalid');
        else
            this.Obj = Obj;

        if (typeof Option == 'undefined' || typeof Option.Type == 'undefined' || Option.Type == 'ClockTimer') {
            this.InitTime = 0;
            this.TimerStopTime = 10;
        }
        else if (Option.Type == 'CountDown') {
            this.InitTime = 20;
        }
        if (typeof Option != 'undefined') {
            this.RepeatTimer = typeof Option.RepeatTimer != 'undefined' ? Option.RepeatTimer : false;
            this.TimeUp = typeof Option.TimeUp == 'function' ? Option.TimeUp : null;
            this.InitTime = typeof Option.InitTime != 'undefined' ? Option.InitTime : this.InitTime;
            this.TimerStopTime = typeof Option.TimerStopTime != 'undefined' ? Option.TimerStopTime : this.TimerStopTime;
        }

        Obj.html(this.DisplayTime);

        if (typeof this.Option != 'undefined' && typeof this.Option.StopResume != 'undefined' && this.Option.StopResume) {
            this.Pause = $('<i class="fa fa-pause" id="btnPause" aria-hidden="true" style="margin-left:5px;" ></i>');
            this.Obj.append(this.Pause);
            this.Resume = $('<i class="fa fa-play" id="btnResume" aria-hidden="true" style="display:none;margin-left:5px;" ></i>');
            this.Obj.append(this.Resume);
            let thisObj = this;
            this.Pause.click(function () {
                thisObj.Flag = true;
                thisObj.Pause.hide();
                thisObj.Resume.show();
            });
            this.Resume.click(function () {
                thisObj.Flag = false;;
                thisObj.Pause.show();
                thisObj.Resume.hide();
            });
        }


        if (typeof Option == 'undefined' || typeof Option.Type == 'undefined' || Option.Type == 'ClockTimer') {
            this.timerStart = this.ClockTimer;
        }
        else if (Option.Type == 'CountDown') {
            this.timerStart = this.CountDown;
        }
        else if (Option.Type == 'Clock') {
            this.timerStart = this.Clock;
        }
        this.timerStart(this);
    }
    CountDown(thisObj) {
        thisObj.totalSec = thisObj.InitTime;
        thisObj.Timer = setInterval(function () {
            if (!thisObj.Flag) {
                thisObj.DisplayTime.html(thisObj.calculateTime(thisObj.totalSec));
                if (thisObj.totalSec == 0) {
                    if (thisObj.TimeUp != null)
                        thisObj.TimeUp();
                    if (thisObj.RepeatTimer) {
                        thisObj.totalSec = thisObj.InitTime;
                    }
                    else {
                        clearInterval(thisObj.Timer);
                    }
                }
                else
                    thisObj.totalSec--;
            }
        }, 1000);

    }
    ClockTimer(thisObj) {
        thisObj.totalSec = 0;
        thisObj.Timer = setInterval(function () {
            if (!thisObj.Flag) { //do something if not paused
                thisObj.DisplayTime.html(thisObj.calculateTime(thisObj.totalSec));
                if (thisObj.totalSec == thisObj.TimerStopTime) {
                    if (thisObj.TimeUp != null)
                        thisObj.TimeUp();
                    if (thisObj.RepeatTimer) {
                        thisObj.totalSec = 0;
                    }
                    else {
                        clearInterval(thisObj.Timer);

                    }
                }
                else
                    thisObj.totalSec++;
            }
        }, 1000);

    }
    Clock(thisObj) {
        thisObj.Timer = setInterval(function () {
            if (!thisObj.Flag) { //do something if not paused
                let date = new Date();
                let hrs = date.getHours();
                let mins = date.getMinutes();
                let secs = date.getSeconds();
                if (hrs > 12)
                    hrs = hrs - 12;
                thisObj.DisplayTime.html(hrs + ':' + mins + ':' + secs);
            }
        }, 1000);

    }
    RemoveTimer() {
        if (this.Timer != null) {
            clearInterval(this.Timer);
            this.Obj.html('');
        }
    }
    StopTimer() {
        if (this.Timer != null) {
            //clearInterval(this.Timer);
            this.DisplayTime.html('00:00:00');
            this.Flag = true;
            if (this.Pause != null)
                this.Pause.hide();
            if (this.Resume != null)
                this.Resume.hide();
        }
    }
    StartTimer() {
        if (this.Timer != null) {
            //clearInterval(this.Timer);
            this.DisplayTime.html('00:00:00');
            this.Flag = false;
            if (this.Pause != null)
                this.Pause.show();
        }
    }
    calculateTime(secs) {
        var t = new Date(1970, 0, 1);
        t.setSeconds(secs);
        var s = t.toTimeString().substr(0, 8);
        if (secs > 86399)
            s = Math.floor((t - Date.parse("1/1/70")) / 3600000) + s.substr(2);
        return s;
    }

}

class Vikas {
    static Success(msg, OnClose) {
        let Modal = new VikasModal();
        Modal.Alert(msg, OnClose, 'Success');
    }
    static Warning(msg, OnClose) {
        let Modal = new VikasModal();
        Modal.Alert(msg, OnClose, 'Warning');
    }
    static Error(msg, OnClose) {
        let Modal = new VikasModal();
        Modal.Alert(msg, OnClose, 'Error');
    }
    static Confirm(msg, OnOK, OnCancel) {
        let Modal = new VikasModal();
        Modal.Confirm(msg, OnOK, OnCancel);
    }
    static PopUp(Content, Title) {
        let Modal = new VikasModal();
        Modal.PopUp(Content, Title);
    }
}
$.fn.VikasTimer = function (options) {
    return new VikasTimer($(this), options);
};