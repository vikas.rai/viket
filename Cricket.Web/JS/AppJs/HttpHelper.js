﻿class HttpHelper {
    static HttpRequests = [];
    static Send(Url, Method, Headers, Params, isText) {
        var controller = new AbortController();
        var signal = controller.signal;
        this.HttpRequests.push(controller);
        Url = new URL(Url);
        if (Method.toLowerCase() == 'get' && Params) {
            Object.keys(Params).forEach(key => Url.searchParams.append(key, Params[key]))
        }
        if (typeof ExtraHeaders == "function") {
            ExtraHeaders(Headers);
        }
        return fetch(Url, {
            method: Method,
            signal: signal,
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            headers: Headers,
            body: Method.toLowerCase() == 'get' ? undefined : Params // body data type must match "Content-Type" header
        }).then(function (response) {
            if (!response.ok) {
                throw new Error(`HTTP error! status: ${response.status}`);
            }
            if (isText) {
                return response.text();
            }
            else {
                return response.json();
            }
        }).catch(function (e) {
            if (e.name === 'AbortError') {
                console.log('Fetch aborted');
            } else {
                throw e;
            }
        })
    }
    static Get(Url, Params) {
        var Headers = {}
        return this.Send(Url, 'GET', Headers, Params);
    }
    static GetHtml(Url, Params) {
        var Headers = {}
        return this.Send(Url, 'GET', Headers, Params, true);
    }
    static Post(Url, Params) {
        //var Headers = { 'Content-Type': 'application/json' }
        var Headers = {};
        return this.Send(Url, 'POST', Headers, Params);
    }
    static PostWithFile(Url, Params) {
        return this.Send(Url, 'POST', Params);
    }
    static KillAllRequest() {
        this.HttpRequests.forEach(x => x.abort());
    }
}
