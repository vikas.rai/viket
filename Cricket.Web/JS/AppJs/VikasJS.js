class VikasJS {
    constructor(_defaultPage) {
        this.defaultPage = _defaultPage;
        this.currentPageClass = null;
        this.JsFiles = [];
        var _this = this;
        VikasJS.Ready(function () {
            _this.LoadUrl();
        })
        window.addEventListener("hashchange", function () {
            _this.LoadUrl();
        });        //lftMenu(VikasJS.RenderMenuFromList(VikasJS.convert(Menu)));
    }
    GetUrl() {
        let hash = location.hash.replace('#', '');
        let queryParams = "";
        if (hash == '') {
            hash = this.defaultPage;
        }
        if (hash.includes('?')) {
            queryParams = hash.split('?')[1];
            hash = hash.split('?')[0];
        }
        let url = location.origin + '/' + hash;
        url = url.split('?')[0].endsWith('.html') ? url : url + '.html'
        return url + "?" + queryParams;
    }
    static getQueryString(name) {
        let hash = location.hash.replace('#', '');
        let queryParams = hash.split('?')[1];
        var uri = new URL("https://example.com?" + queryParams);
        return uri.searchParams.get(name);
    }
    LoadUrl() {
        HttpHelper.KillAllRequest();
        let _self = this;
        let Url = _self.GetUrl();
        let V_DOM = document.querySelector('[vikas-js]');
        if (_self.currentPageClass && _self.currentPageClass.destory) {
            _self.currentPageClass.destory();
        }
        // V_DOM.
        V_DOM.innerHTML = '<h1><i class="fa fa-cog fa-spin"></i> Loading...</h1>';
        HttpHelper.GetHtml(Url)
            .then(function (data) {
                let El = document.createElement("div");
                El.innerHTML = data;

                let titleEl = El.querySelector('title');

                if (titleEl != null)
                    document.title = titleEl.text;

                let scripts = El.querySelectorAll('script');
                scripts.forEach(element => {
                    if (element.src == '') {
                        setTimeout(function () {
                            try {
                                eval(element.innerHTML)
                            } catch (error) {
                                setTimeout(function () {
                                    eval(element.innerHTML)
                                }, 200);
                            }
                        }, 100);
                    }
                    else {
                        if (!_self.JsFiles.includes(element.src)) {
                            _self.JsFiles.push(element.src);
                            data = data.replace(element.outerHTML, "");
                            let script = document.createElement('script')
                            script.src = element.src + "?v=" + ResourceVersion;
                            document.head.appendChild(script);
                        }
                    }
                });
                let className = El.getElementsByTagName('vikas-js-class')
                if (className.length > 0 && className[0].innerHTML) {
                    data = data.replace(className[0].outerHTML, "");
                    setTimeout(function () {
                        try {
                            eval("_self.currentPageClass = new " + className[0].innerHTML + "()");
                        } catch (error) {
                            setTimeout(function () {
                                eval("_self.currentPageClass = new " + className[0].innerHTML + "()");
                            }, 200);
                        }
                    }, 100);
                }
                V_DOM.innerHTML = data;
            })
            .catch(function (error) {
                V_DOM.innerHTML = `<h1>${error}</h1>`;
            })
    }
    static Ready(callback) {
        if (document.readyState === "complete" || (document.readyState !== "loading" && !document.documentElement.doScroll)) {
            callback();
        } else {
            document.addEventListener("DOMContentLoaded", callback);
        }
    }
    static RenderMenuFromList(lst) {
        let mnu = '<ul> ';
        $.each(lst, function () {
            if (this.items.length > 0) {
                mnu += '<li style="margin-left:8px;"><span><i class="icon  fa ' + this.Class + ' " style="font-size:22px!important"></i> ' + this.Name + '</span>';
                mnu += VikasJS.RenderMenuFromList(this.items);
                mnu += '</li>';
            }
            else {
                mnu += '<li style="margin-left:8px;"><a  href="/Home/VikasJS' + this.Href + '"><i class="icon  fa ' + this.Class + '" style="font-size:22px!important"></i> ' + this.Name + ' </a></li>';
            }
        });
        return mnu + '</ul>';
    }
    static convert(array) {
        let map = {};
        for (let i = 0; i < array.length; i++) {
            let obj = array[i];
            obj.items = [];
            map[obj.MenuId] = obj;
            let parent = obj.ParentId || '-';
            if (!map[parent]) {
                map[parent] = {
                    items: []
                };
            }
            map[parent].items.push(obj);
        }
        return map['-'].items;
    }
    static menuToggle() {
        if ($('#menuToggle').attr('data-toggle') == "false") {
            $('nav#menu').trigger('open.mm');
            $('#menuToggle').attr('data-toggle', true);
        }
        else {
            $('nav#menu').trigger('close.mm');
            $('#menuToggle').attr('data-toggle', false);
        }
    }
    static ScrollContent(selector) {
        let scroll = $(selector).niceScroll({ touchbehavior: false, cursorcolor: "#0000FF", cursoropacitymax: 0.4, cursorwidth: 10 });
    }
    static ToggleListing() {
        //Voice.JS(JSVoice);
        if (artyom.isRecognizing()) {
            voice.OFF();
            $('.fa-microphone').removeClass('Listen');
        }
        else {
            voice.ON();
            $('.fa-microphone').addClass('Listen');
        }
    }
    static JSVoice(Text) {
        Vikas.PopUp(Text)
    }

}


