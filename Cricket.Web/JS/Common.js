class Common {
    constructor() {
    }
   static getTeamImageURL(image) {
        let baseURL = "https://img1.hscicdn.com/image/upload/f_auto,t_ds_square_w_160,q_50/lsci";
        return (image == null || image.url == undefined || image.url == "") ? "/Images/no-team-logo.svg" : baseURL + image.url;
    }
}