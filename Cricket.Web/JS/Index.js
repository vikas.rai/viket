
var ResourceVersion="1.03"
document.addEventListener('click', function (e) {
  // Hamburger menu
  if (e.target.classList.contains('hamburger-toggle')) {
    e.target.children[0].classList.toggle('active');
  }
})

function getApiURL(url) {
    return `https://localhost:5001/api/${url}`;
}