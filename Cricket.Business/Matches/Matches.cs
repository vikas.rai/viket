using Cricket.Business.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace Cricket.Business.Matches
{
    public class Matches
    {
        public async Task<List<Match>> GetCurrent()
        {
            List<Match> matches = new List<Match>();
            HttpClient Client = new HttpClient();
            Client.BaseAddress = new Uri("https://hs-consumer-api.espncricinfo.com");
            //Client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            var Response = await Client.GetAsync("v1/pages/matches/current?lang=en&latest=true");
            string ResponseData = "";
            if (Response.StatusCode == HttpStatusCode.OK)
            {
                ResponseData = await Response.Content.ReadAsStringAsync();
                var liveMatch = JsonConvert.DeserializeObject<MatchContent>(ResponseData, JsonSettings.IgnoreNull);
                matches = liveMatch.Matches;
            }
            else
            {
                ResponseData = await Response.Content.ReadAsStringAsync();
                throw new Exception(ResponseData);
            }
            return matches;
        }
        public async Task<List<Match>> GetTrending()
        {
            List<Match> matches = new List<Match>();
            HttpClient Client = new HttpClient();
            Client.BaseAddress = new Uri("https://hs-consumer-api.espncricinfo.com");
            //Client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            var Response = await Client.GetAsync("v1/edition/details?lang=en&edition=in&trendingMatches=true");
            string ResponseData = "";
            if (Response.StatusCode == HttpStatusCode.OK)
            {
                ResponseData = await Response.Content.ReadAsStringAsync();
                var liveMatch = JsonConvert.DeserializeObject<LiveMatch>(ResponseData, JsonSettings.IgnoreNull);
                matches = liveMatch.trendingMatches.Matches;
            }
            else
            {
                ResponseData = await Response.Content.ReadAsStringAsync();
                throw new Exception(ResponseData);
            }
            return matches;
        }

        public async Task<List<Match>> GetScheduled()
        {
            List<Match> matches = new List<Match>();
            HttpClient Client = new HttpClient();
            Client.BaseAddress = new Uri("https://hs-consumer-api.espncricinfo.com");
            //Client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            var Response = await Client.GetAsync("v1/pages/matches/scheduled?lang=en");
            string ResponseData = "";
            if (Response.StatusCode == HttpStatusCode.OK)
            {
                ResponseData = await Response.Content.ReadAsStringAsync();
                var liveMatch = JsonConvert.DeserializeObject<LiveMatch>(ResponseData, JsonSettings.IgnoreNull);
                matches = liveMatch.Content.Matches;
            }
            else
            {
                ResponseData = await Response.Content.ReadAsStringAsync();
                throw new Exception(ResponseData);
            }
            return matches;
        }

        public async Task<List<Match>> Details(long SeriesID, long MatchID)
        {
            List<Match> matches = new List<Match>();
            HttpClient Client = new HttpClient();
            Client.BaseAddress = new Uri("https://hs-consumer-api.espncricinfo.com");
            //Client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            var Response = await Client.GetAsync($"v1/pages/match/details?lang=en&seriesId={SeriesID}&matchId={MatchID}&latest=true");
            string ResponseData = "";
            if (Response.StatusCode == HttpStatusCode.OK)
            {
                ResponseData = await Response.Content.ReadAsStringAsync();
                var liveMatch = JsonConvert.DeserializeObject<LiveMatch>(ResponseData, JsonSettings.IgnoreNull);
                matches = liveMatch.Content.Matches;
            }
            else
            {
                ResponseData = await Response.Content.ReadAsStringAsync();
                throw new Exception(ResponseData);
            }
            return matches;
        }

        public async Task<ScorecardModel> ScoreCard(long SeriesID, long MatchID)
        {
            ScorecardModel Scorecard = new ScorecardModel();
            HttpClient Client = new HttpClient();
            Client.BaseAddress = new Uri("https://hs-consumer-api.espncricinfo.com");
            //Client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            var Response = await Client.GetAsync($"v1/pages/match/scorecard?lang=en&seriesId={SeriesID}&matchId={MatchID}");
            string ResponseData = "";
            if (Response.StatusCode == HttpStatusCode.OK)
            {
                ResponseData = await Response.Content.ReadAsStringAsync();
                Scorecard = JsonConvert.DeserializeObject<ScorecardModel>(ResponseData, JsonSettings.IgnoreNull);
            }
            else
            {
                ResponseData = await Response.Content.ReadAsStringAsync();
                throw new Exception(ResponseData);
            }
            return Scorecard;
        }

        public async Task<LiveMatchModel> GetLiveMatch(long SeriesID, long MatchID)
        {
            LiveMatchModel liveMatch = new LiveMatchModel();
            HttpClient Client = new HttpClient();
            Client.BaseAddress = new Uri("https://hs-consumer-api.espncricinfo.com");
            //Client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            var Response = await Client.GetAsync($"v1/pages/match/home?lang=en&seriesId={SeriesID}&matchId={MatchID}");
            string ResponseData = "";
            if (Response.StatusCode == HttpStatusCode.OK)
            {
                ResponseData = await Response.Content.ReadAsStringAsync();
                liveMatch = JsonConvert.DeserializeObject<LiveMatchModel>(ResponseData, JsonSettings.IgnoreNull);
            }
            else
            {
                ResponseData = await Response.Content.ReadAsStringAsync();
                throw new Exception(ResponseData);
            }
            return liveMatch;
        }

        public async Task<StoryDetails> Preview(long SeriesID, long MatchID)
        {
            ScorecardModel Scorecard = new ScorecardModel();
            HttpClient Client = new HttpClient();
            Client.BaseAddress = new Uri("https://hs-consumer-api.espncricinfo.com");
            //Client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            var Response = await Client.GetAsync($"v1/pages/match/preview?lang=en&seriesId={SeriesID}&matchId={MatchID}");
            string ResponseData = "";
            if (Response.StatusCode == HttpStatusCode.OK)
            {
                ResponseData = await Response.Content.ReadAsStringAsync();
                Scorecard = JsonConvert.DeserializeObject<ScorecardModel>(ResponseData, JsonSettings.IgnoreNull);
            }
            else
            {
                ResponseData = await Response.Content.ReadAsStringAsync();
                throw new Exception(ResponseData);
            }
            return Scorecard.Content.StoryDetails;
        }
        public async Task<List<TeamPlayer>> Squad(long SeriesID, long MatchID)
        {
            ScorecardModel Scorecard = new ScorecardModel();
            HttpClient Client = new HttpClient();
            Client.BaseAddress = new Uri("https://hs-consumer-api.espncricinfo.com");
            //Client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            var Response = await Client.GetAsync($"v1/pages/match/squad-players?lang=en&seriesId={SeriesID}&matchId={MatchID}");
            string ResponseData = "";
            if (Response.StatusCode == HttpStatusCode.OK)
            {
                ResponseData = await Response.Content.ReadAsStringAsync();
                Scorecard = JsonConvert.DeserializeObject<ScorecardModel>(ResponseData, JsonSettings.IgnoreNull);
            }
            else
            {
                ResponseData = await Response.Content.ReadAsStringAsync();
                throw new Exception(ResponseData);
            }
            return Scorecard.Content.MatchPlayers.TeamPlayers;
        }

        public async Task<Tuple<MatchPlayers, Dictionary<string, List<BenchPlayer>>>> PlayingXI(long SeriesID, long MatchID)
        {
            ScorecardModel Scorecard = new ScorecardModel();
            HttpClient Client = new HttpClient();
            Client.BaseAddress = new Uri("https://hs-consumer-api.espncricinfo.com");
            //Client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            var Response = await Client.GetAsync($"v1/pages/match/team-players?lang=en&seriesId={SeriesID}&matchId={MatchID}");
            string ResponseData = "";
            if (Response.StatusCode == HttpStatusCode.OK)
            {
                ResponseData = await Response.Content.ReadAsStringAsync();
                Scorecard = JsonConvert.DeserializeObject<ScorecardModel>(ResponseData, JsonSettings.IgnoreNull);
            }
            else
            {
                ResponseData = await Response.Content.ReadAsStringAsync();
                throw new Exception(ResponseData);
            }
            return Tuple.Create(Scorecard.Content.MatchPlayers, Scorecard.Content.BenchPlayers);
        }

        public async Task<StoryDetails> Fantasy(long SeriesID, long MatchID)
        {
            ScorecardModel Scorecard = new ScorecardModel();
            HttpClient Client = new HttpClient();
            Client.BaseAddress = new Uri("https://hs-consumer-api.espncricinfo.com");
            //Client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            var Response = await Client.GetAsync($"v1/pages/match/fantasy-pick?lang=en&seriesId={SeriesID}&matchId={MatchID}");
            string ResponseData = "";
            if (Response.StatusCode == HttpStatusCode.OK)
            {
                ResponseData = await Response.Content.ReadAsStringAsync();
                Scorecard = JsonConvert.DeserializeObject<ScorecardModel>(ResponseData, JsonSettings.IgnoreNull);
            }
            else
            {
                ResponseData = await Response.Content.ReadAsStringAsync();
                throw new Exception(ResponseData);
            }
            return Scorecard.Content.StoryDetails;
        }
    }
}