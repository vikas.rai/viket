using System;
using System.Collections.Generic;

using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Cricket.Business.Models
{
    public static class JsonSettings
    {
        public static readonly JsonSerializerSettings IgnoreNull =
        new JsonSerializerSettings()
        {
            // ReferenceLoopHandling = ReferenceLoopHandling.,
            NullValueHandling = NullValueHandling.Ignore
        };
    }
    public partial class LiveMatch
    {
        [JsonProperty("content", NullValueHandling = NullValueHandling.Ignore)]
        public MatchContent Content { get; set; }

        [JsonProperty("trendingMatches", NullValueHandling = NullValueHandling.Ignore)]
        public MatchContent trendingMatches { get; set; }
    }

    public partial class MatchContent
    {
        [JsonProperty("matches", NullValueHandling = NullValueHandling.Ignore)]
        public List<Match> Matches { get; set; }

        [JsonProperty("match", NullValueHandling = NullValueHandling.Ignore)]
        public Match Match { get; set; }
    }
    public partial class Match
    {
        [JsonProperty("_uid", NullValueHandling = NullValueHandling.Ignore)]
        public long? Uid { get; set; }

        [JsonProperty("id", NullValueHandling = NullValueHandling.Ignore)]
        public long? Id { get; set; }

        [JsonProperty("objectId", NullValueHandling = NullValueHandling.Ignore)]
        public long? ObjectId { get; set; }

        [JsonProperty("scribeId", NullValueHandling = NullValueHandling.Ignore)]
        public long? ScribeId { get; set; }

        [JsonProperty("slug", NullValueHandling = NullValueHandling.Ignore)]
        public string Slug { get; set; }

        [JsonProperty("stage", NullValueHandling = NullValueHandling.Ignore)]
        public string Stage { get; set; }

        [JsonProperty("state", NullValueHandling = NullValueHandling.Ignore)]
        public string State { get; set; }

        [JsonProperty("title", NullValueHandling = NullValueHandling.Ignore)]
        public string Title { get; set; }

        [JsonProperty("floodlit", NullValueHandling = NullValueHandling.Ignore)]
        public string Floodlit { get; set; }

        [JsonProperty("startDate", NullValueHandling = NullValueHandling.Ignore)]
        public DateTimeOffset? StartDate { get; set; }

        [JsonProperty("endDate", NullValueHandling = NullValueHandling.Ignore)]
        public DateTimeOffset? EndDate { get; set; }

        [JsonProperty("startTime", NullValueHandling = NullValueHandling.Ignore)]
        public DateTimeOffset? StartTime { get; set; }

        [JsonProperty("timePublished", NullValueHandling = NullValueHandling.Ignore)]
        public bool? TimePublished { get; set; }

        [JsonProperty("scheduleNote", NullValueHandling = NullValueHandling.Ignore)]
        public string ScheduleNote { get; set; }

        [JsonProperty("isCancelled", NullValueHandling = NullValueHandling.Ignore)]
        public bool? IsCancelled { get; set; }

        [JsonProperty("status", NullValueHandling = NullValueHandling.Ignore)]
        public string Status { get; set; }

        [JsonProperty("statusText", NullValueHandling = NullValueHandling.Ignore)]
        public string StatusText { get; set; }

        [JsonProperty("winnerTeamId")]
        public object WinnerTeamId { get; set; }

        [JsonProperty("tossWinnerTeamId")]
        public long? TossWinnerTeamId { get; set; }

        [JsonProperty("tossWinnerChoice")]
        public long? TossWinnerChoice { get; set; }

        [JsonProperty("resultStatus")]
        public object ResultStatus { get; set; }

        [JsonProperty("liveInning")]
        public long? LiveInning { get; set; }

        [JsonProperty("liveInningPredictions")]
        public object LiveInningPredictions { get; set; }

        [JsonProperty("series", NullValueHandling = NullValueHandling.Ignore)]
        public Series Series { get; set; }

        [JsonProperty("ground", NullValueHandling = NullValueHandling.Ignore)]
        public Ground Ground { get; set; }

        [JsonProperty("teams", NullValueHandling = NullValueHandling.Ignore)]
        public List<TeamElement> Teams { get; set; }

        [JsonProperty("dayType", NullValueHandling = NullValueHandling.Ignore)]
        public string DayType { get; set; }

        [JsonProperty("format", NullValueHandling = NullValueHandling.Ignore)]
        public string Format { get; set; }

        [JsonProperty("previewStoryId")]
        public long? PreviewStoryId { get; set; }

        [JsonProperty("reportStoryId")]
        public long? ReportStoryId { get; set; }

        [JsonProperty("liveBlogStoryId")]
        public object LiveBlogStoryId { get; set; }

        [JsonProperty("fantasyPickStoryId")]
        public long? FantasyPickStoryId { get; set; }

        [JsonProperty("drawOdds")]
        public Odds DrawOdds { get; set; }

        [JsonProperty("isSuperOver", NullValueHandling = NullValueHandling.Ignore)]
        public bool? IsSuperOver { get; set; }

        [JsonProperty("languages", NullValueHandling = NullValueHandling.Ignore)]
        public List<string> Languages { get; set; }

        [JsonProperty("generatedAt", NullValueHandling = NullValueHandling.Ignore)]
        public DateTimeOffset? GeneratedAt { get; set; }

        [JsonProperty("umpires", NullValueHandling = NullValueHandling.Ignore)]
        public List<MatchReferee> Umpires { get; set; }

        [JsonProperty("tvUmpires", NullValueHandling = NullValueHandling.Ignore)]
        public List<MatchReferee> TvUmpires { get; set; }

        [JsonProperty("reserveUmpires", NullValueHandling = NullValueHandling.Ignore)]
        public List<MatchReferee> ReserveUmpires { get; set; }

        [JsonProperty("matchReferees", NullValueHandling = NullValueHandling.Ignore)]
        public List<MatchReferee> MatchReferees { get; set; }
    }

    public partial class Odds
    {
        [JsonProperty("odds", NullValueHandling = NullValueHandling.Ignore)]
        public string OddsOdds { get; set; }

        [JsonProperty("url", NullValueHandling = NullValueHandling.Ignore)]
        public Uri Url { get; set; }
    }

    public partial class Ground
    {
        [JsonProperty("id", NullValueHandling = NullValueHandling.Ignore)]
        public long? Id { get; set; }

        [JsonProperty("objectId", NullValueHandling = NullValueHandling.Ignore)]
        public long? ObjectId { get; set; }

        [JsonProperty("name", NullValueHandling = NullValueHandling.Ignore)]
        public string Name { get; set; }

        [JsonProperty("smallName", NullValueHandling = NullValueHandling.Ignore)]
        public string SmallName { get; set; }

        [JsonProperty("longName", NullValueHandling = NullValueHandling.Ignore)]
        public string LongName { get; set; }

        [JsonProperty("location", NullValueHandling = NullValueHandling.Ignore)]
        public string Location { get; set; }

        [JsonProperty("town", NullValueHandling = NullValueHandling.Ignore)]
        public Town Town { get; set; }

        [JsonProperty("country", NullValueHandling = NullValueHandling.Ignore)]
        public Country Country { get; set; }
    }

    public partial class Country
    {
        [JsonProperty("id", NullValueHandling = NullValueHandling.Ignore)]
        public long? Id { get; set; }

        [JsonProperty("objectId", NullValueHandling = NullValueHandling.Ignore)]
        public long? ObjectId { get; set; }

        [JsonProperty("name", NullValueHandling = NullValueHandling.Ignore)]
        public string Name { get; set; }

        [JsonProperty("shortName", NullValueHandling = NullValueHandling.Ignore)]
        public string ShortName { get; set; }
    }

    public partial class Town
    {
        [JsonProperty("id", NullValueHandling = NullValueHandling.Ignore)]
        public long? Id { get; set; }

        [JsonProperty("objectId", NullValueHandling = NullValueHandling.Ignore)]
        public long? ObjectId { get; set; }

        [JsonProperty("name", NullValueHandling = NullValueHandling.Ignore)]
        public string Name { get; set; }

        [JsonProperty("area", NullValueHandling = NullValueHandling.Ignore)]
        public string Area { get; set; }

        [JsonProperty("timezone", NullValueHandling = NullValueHandling.Ignore)]
        public string Timezone { get; set; }
    }

    public partial class Series
    {
        [JsonProperty("id", NullValueHandling = NullValueHandling.Ignore)]
        public long? Id { get; set; }

        [JsonProperty("objectId", NullValueHandling = NullValueHandling.Ignore)]
        public long? ObjectId { get; set; }

        [JsonProperty("scribeId", NullValueHandling = NullValueHandling.Ignore)]
        public long? ScribeId { get; set; }

        [JsonProperty("slug", NullValueHandling = NullValueHandling.Ignore)]
        public string Slug { get; set; }

        [JsonProperty("name", NullValueHandling = NullValueHandling.Ignore)]
        public string Name { get; set; }

        [JsonProperty("longName", NullValueHandling = NullValueHandling.Ignore)]
        public string LongName { get; set; }

        [JsonProperty("alternateName", NullValueHandling = NullValueHandling.Ignore)]
        public string AlternateName { get; set; }

        [JsonProperty("longAlternateName", NullValueHandling = NullValueHandling.Ignore)]
        public string LongAlternateName { get; set; }

        [JsonProperty("year", NullValueHandling = NullValueHandling.Ignore)]
        public long? Year { get; set; }

        [JsonProperty("typeId", NullValueHandling = NullValueHandling.Ignore)]
        public long? TypeId { get; set; }

        [JsonProperty("isTrophy", NullValueHandling = NullValueHandling.Ignore)]
        public bool? IsTrophy { get; set; }

        [JsonProperty("description", NullValueHandling = NullValueHandling.Ignore)]
        public string Description { get; set; }

        [JsonProperty("season", NullValueHandling = NullValueHandling.Ignore)]
        public string Season { get; set; }

        [JsonProperty("startDate", NullValueHandling = NullValueHandling.Ignore)]
        public DateTimeOffset? StartDate { get; set; }

        [JsonProperty("endDate", NullValueHandling = NullValueHandling.Ignore)]
        public DateTimeOffset? EndDate { get; set; }

        [JsonProperty("hasStandings", NullValueHandling = NullValueHandling.Ignore)]
        public bool? HasStandings { get; set; }

        [JsonProperty("totalVideos", NullValueHandling = NullValueHandling.Ignore)]
        public long? TotalVideos { get; set; }
    }

    public partial class TeamElement
    {
        [JsonProperty("team", NullValueHandling = NullValueHandling.Ignore)]
        public TeamTeam Team { get; set; }

        [JsonProperty("isHome", NullValueHandling = NullValueHandling.Ignore)]
        public bool? IsHome { get; set; }

        [JsonProperty("isLive", NullValueHandling = NullValueHandling.Ignore)]
        public bool? IsLive { get; set; }

        [JsonProperty("score")]
        public string Score { get; set; }

        [JsonProperty("scoreInfo")]
        public string ScoreInfo { get; set; }

        [JsonProperty("inningNumbers", NullValueHandling = NullValueHandling.Ignore)]
        public List<long> InningNumbers { get; set; }

        [JsonProperty("points")]
        public object Points { get; set; }

        [JsonProperty("sidePlayers")]
        public object SidePlayers { get; set; }

        [JsonProperty("sideBatsmen")]
        public object SideBatsmen { get; set; }

        [JsonProperty("sideFielders")]
        public object SideFielders { get; set; }

        [JsonProperty("captain")]
        public object Captain { get; set; }

        [JsonProperty("teamOdds")]
        public Odds TeamOdds { get; set; }
    }

    public partial class TeamTeam
    {
        [JsonProperty("id", NullValueHandling = NullValueHandling.Ignore)]
        public long? Id { get; set; }

        [JsonProperty("objectId", NullValueHandling = NullValueHandling.Ignore)]
        public long? ObjectId { get; set; }

        [JsonProperty("scribeId", NullValueHandling = NullValueHandling.Ignore)]
        public long? ScribeId { get; set; }

        [JsonProperty("slug", NullValueHandling = NullValueHandling.Ignore)]
        public string Slug { get; set; }

        [JsonProperty("name", NullValueHandling = NullValueHandling.Ignore)]
        public string Name { get; set; }

        [JsonProperty("longName", NullValueHandling = NullValueHandling.Ignore)]
        public string LongName { get; set; }

        [JsonProperty("abbreviation", NullValueHandling = NullValueHandling.Ignore)]
        public string Abbreviation { get; set; }

        [JsonProperty("isCountry", NullValueHandling = NullValueHandling.Ignore)]
        public bool? IsCountry { get; set; }

        [JsonProperty("primaryColor")]
        public string PrimaryColor { get; set; }

        [JsonProperty("image", NullValueHandling = NullValueHandling.Ignore)]
        public Image Image { get; set; }
    }

    public partial class Image
    {
        [JsonProperty("id", NullValueHandling = NullValueHandling.Ignore)]
        public long? Id { get; set; }

        [JsonProperty("objectId", NullValueHandling = NullValueHandling.Ignore)]
        public long? ObjectId { get; set; }

        [JsonProperty("slug", NullValueHandling = NullValueHandling.Ignore)]
        public string Slug { get; set; }

        [JsonProperty("url", NullValueHandling = NullValueHandling.Ignore)]
        public string Url { get; set; }

        [JsonProperty("width", NullValueHandling = NullValueHandling.Ignore)]
        public long? Width { get; set; }

        [JsonProperty("height", NullValueHandling = NullValueHandling.Ignore)]
        public long? Height { get; set; }

        [JsonProperty("caption", NullValueHandling = NullValueHandling.Ignore)]
        public string Caption { get; set; }

        [JsonProperty("longCaption", NullValueHandling = NullValueHandling.Ignore)]
        public string LongCaption { get; set; }

        [JsonProperty("credit")]
        public string Credit { get; set; }

        [JsonProperty("photographer")]
        public object Photographer { get; set; }

        [JsonProperty("peerUrls")]
        public PeerUrls PeerUrls { get; set; }

        [JsonProperty("dateTaken", NullValueHandling = NullValueHandling.Ignore)]
        public DateTimeOffset? DateTaken { get; set; }
    }

}