using System;
using System.Collections.Generic;

using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Cricket.Business.Models
{
    public partial class ScorecardModel
    {
        [JsonProperty("match", NullValueHandling = NullValueHandling.Ignore)]
        public Match Match { get; set; }

        [JsonProperty("content", NullValueHandling = NullValueHandling.Ignore)]
        public Content Content { get; set; }
    }

    public partial class Content
    {
        [JsonProperty("scorecard", NullValueHandling = NullValueHandling.Ignore)]
        public Scorecard Scorecard { get; set; }

        [JsonProperty("matchPlayers", NullValueHandling = NullValueHandling.Ignore)]
        public MatchPlayers MatchPlayers { get; set; }

        [JsonProperty("benchPlayers", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, List<BenchPlayer>> BenchPlayers { get; set; }

        [JsonProperty("notes", NullValueHandling = NullValueHandling.Ignore)]
        public Notes Notes { get; set; }

        [JsonProperty("closeOfPlay")]
        public object CloseOfPlay { get; set; }

        [JsonProperty("supportInfo", NullValueHandling = NullValueHandling.Ignore)]
        public SupportInfo SupportInfo { get; set; }

        [JsonProperty("storyDetails", NullValueHandling = NullValueHandling.Ignore)]
        public StoryDetails StoryDetails { get; set; }
    }
    public partial class BenchPlayer
    {
        [JsonProperty("playerRoleType", NullValueHandling = NullValueHandling.Ignore)]
        public string PlayerRoleType { get; set; }

        [JsonProperty("player", NullValueHandling = NullValueHandling.Ignore)]
        public Player1Class Player { get; set; }

        [JsonProperty("isOverseas", NullValueHandling = NullValueHandling.Ignore)]
        public bool? IsOverseas { get; set; }

        [JsonProperty("isWithdrawn", NullValueHandling = NullValueHandling.Ignore)]
        public bool? IsWithdrawn { get; set; }

        [JsonProperty("note")]
        public object Note { get; set; }
    }

    public partial class MatchPlayers
    {
        [JsonProperty("teamPlayers", NullValueHandling = NullValueHandling.Ignore)]
        public List<TeamPlayer> TeamPlayers { get; set; }
    }

    public partial class TeamPlayer
    {
        [JsonProperty("type", NullValueHandling = NullValueHandling.Ignore)]
        public string Type { get; set; }

        [JsonProperty("team", NullValueHandling = NullValueHandling.Ignore)]
        public TeamInfoClass Team { get; set; }

        [JsonProperty("players", NullValueHandling = NullValueHandling.Ignore)]
        public List<PlayerElement> Players { get; set; }

        [JsonProperty("bestBatsmen", NullValueHandling = NullValueHandling.Ignore)]
        public List<BestBatsman> BestBatsmen { get; set; }

        [JsonProperty("bestBowlers", NullValueHandling = NullValueHandling.Ignore)]
        public List<BestBowler> BestBowlers { get; set; }
    }

    public partial class BestBatsman
    {
        [JsonProperty("player", NullValueHandling = NullValueHandling.Ignore)]
        public Player1Class Player { get; set; }

        [JsonProperty("matches", NullValueHandling = NullValueHandling.Ignore)]
        public long? Matches { get; set; }

        [JsonProperty("runs", NullValueHandling = NullValueHandling.Ignore)]
        public long? Runs { get; set; }

        [JsonProperty("innings", NullValueHandling = NullValueHandling.Ignore)]
        public long? Innings { get; set; }

        [JsonProperty("average", NullValueHandling = NullValueHandling.Ignore)]
        public double? Average { get; set; }

        [JsonProperty("notouts", NullValueHandling = NullValueHandling.Ignore)]
        public long? Notouts { get; set; }

        [JsonProperty("strikerate", NullValueHandling = NullValueHandling.Ignore)]
        public double? Strikerate { get; set; }
    }

    public partial class Player1Class
    {
        [JsonProperty("id", NullValueHandling = NullValueHandling.Ignore)]
        public long? Id { get; set; }

        [JsonProperty("objectId", NullValueHandling = NullValueHandling.Ignore)]
        public long? ObjectId { get; set; }

        [JsonProperty("name", NullValueHandling = NullValueHandling.Ignore)]
        public string Name { get; set; }

        [JsonProperty("longName", NullValueHandling = NullValueHandling.Ignore)]
        public string LongName { get; set; }

        [JsonProperty("mobileName", NullValueHandling = NullValueHandling.Ignore)]
        public string MobileName { get; set; }

        [JsonProperty("indexName", NullValueHandling = NullValueHandling.Ignore)]
        public string IndexName { get; set; }

        [JsonProperty("slug", NullValueHandling = NullValueHandling.Ignore)]
        public string Slug { get; set; }

        [JsonProperty("battingName", NullValueHandling = NullValueHandling.Ignore)]
        public string BattingName { get; set; }

        [JsonProperty("fieldingName", NullValueHandling = NullValueHandling.Ignore)]
        public string FieldingName { get; set; }

        [JsonProperty("gender", NullValueHandling = NullValueHandling.Ignore)]
        public string Gender { get; set; }

        [JsonProperty("playingRole", NullValueHandling = NullValueHandling.Ignore)]
        public string PlayingRole { get; set; }

        [JsonProperty("battingStyles", NullValueHandling = NullValueHandling.Ignore)]
        public List<string> BattingStyles { get; set; }

        [JsonProperty("bowlingStyles", NullValueHandling = NullValueHandling.Ignore)]
        public List<string> BowlingStyles { get; set; }

        [JsonProperty("longBattingStyles", NullValueHandling = NullValueHandling.Ignore)]
        public List<string> LongBattingStyles { get; set; }

        [JsonProperty("longBowlingStyles", NullValueHandling = NullValueHandling.Ignore)]
        public List<string> LongBowlingStyles { get; set; }

        [JsonProperty("image")]
        public Image Image { get; set; }

        [JsonProperty("countryTeamId", NullValueHandling = NullValueHandling.Ignore)]
        public long? CountryTeamId { get; set; }

        [JsonProperty("playingRoleType", NullValueHandling = NullValueHandling.Ignore)]
        public string PlayingRoleType { get; set; }

        [JsonProperty("playingRoles", NullValueHandling = NullValueHandling.Ignore)]
        public List<string> PlayingRoles { get; set; }

        [JsonProperty("dateOfBirth", NullValueHandling = NullValueHandling.Ignore)]
        public DateOfBirth DateOfBirth { get; set; }

        [JsonProperty("dateOfDeath")]
        public object DateOfDeath { get; set; }
    }

    public partial class DateOfBirth
    {
        [JsonProperty("year", NullValueHandling = NullValueHandling.Ignore)]
        public long? Year { get; set; }

        [JsonProperty("month", NullValueHandling = NullValueHandling.Ignore)]
        public long? Month { get; set; }

        [JsonProperty("date", NullValueHandling = NullValueHandling.Ignore)]
        public long? Date { get; set; }
    }

    public partial class PeerUrls
    {
        [JsonProperty("FILM", NullValueHandling = NullValueHandling.Ignore)]
        public string Film { get; set; }

        [JsonProperty("WIDE", NullValueHandling = NullValueHandling.Ignore)]
        public string Wide { get; set; }

        [JsonProperty("SQUARE", NullValueHandling = NullValueHandling.Ignore)]
        public string Square { get; set; }
    }

    public partial class BestBowler
    {
        [JsonProperty("player", NullValueHandling = NullValueHandling.Ignore)]
        public Player1Class Player { get; set; }

        [JsonProperty("matches", NullValueHandling = NullValueHandling.Ignore)]
        public long? Matches { get; set; }

        [JsonProperty("wickets", NullValueHandling = NullValueHandling.Ignore)]
        public long? Wickets { get; set; }

        [JsonProperty("innings", NullValueHandling = NullValueHandling.Ignore)]
        public long? Innings { get; set; }

        [JsonProperty("average", NullValueHandling = NullValueHandling.Ignore)]
        public double? Average { get; set; }

        [JsonProperty("economy", NullValueHandling = NullValueHandling.Ignore)]
        public double? Economy { get; set; }

        [JsonProperty("conceded", NullValueHandling = NullValueHandling.Ignore)]
        public long? Conceded { get; set; }

        [JsonProperty("balls", NullValueHandling = NullValueHandling.Ignore)]
        public long? Balls { get; set; }

        [JsonProperty("strikerate", NullValueHandling = NullValueHandling.Ignore)]
        public double? Strikerate { get; set; }
    }

    public partial class PlayerElement
    {
        [JsonProperty("playerRoleType", NullValueHandling = NullValueHandling.Ignore)]
        public string PlayerRoleType { get; set; }

        [JsonProperty("player", NullValueHandling = NullValueHandling.Ignore)]
        public Player1Class Player { get; set; }

        [JsonProperty("isOverseas", NullValueHandling = NullValueHandling.Ignore)]
        public bool? IsOverseas { get; set; }

        [JsonProperty("isWithdrawn", NullValueHandling = NullValueHandling.Ignore)]
        public bool? IsWithdrawn { get; set; }

        [JsonProperty("note")]
        public object Note { get; set; }
    }

    public partial class TeamInfoClass
    {
        [JsonProperty("id", NullValueHandling = NullValueHandling.Ignore)]
        public long? Id { get; set; }

        [JsonProperty("objectId", NullValueHandling = NullValueHandling.Ignore)]
        public long? ObjectId { get; set; }

        [JsonProperty("scribeId", NullValueHandling = NullValueHandling.Ignore)]
        public long? ScribeId { get; set; }

        [JsonProperty("slug", NullValueHandling = NullValueHandling.Ignore)]
        public string Slug { get; set; }

        [JsonProperty("name", NullValueHandling = NullValueHandling.Ignore)]
        public string Name { get; set; }

        [JsonProperty("longName", NullValueHandling = NullValueHandling.Ignore)]
        public string LongName { get; set; }

        [JsonProperty("abbreviation", NullValueHandling = NullValueHandling.Ignore)]
        public string Abbreviation { get; set; }

        [JsonProperty("isCountry", NullValueHandling = NullValueHandling.Ignore)]
        public bool? IsCountry { get; set; }

        [JsonProperty("primaryColor")]
        public string PrimaryColor { get; set; }

        [JsonProperty("image", NullValueHandling = NullValueHandling.Ignore)]
        public Image Image { get; set; }
    }

    public partial class Notes
    {
        [JsonProperty("type", NullValueHandling = NullValueHandling.Ignore)]
        public string Type { get; set; }

        [JsonProperty("groups", NullValueHandling = NullValueHandling.Ignore)]
        public List<NotesGroup> Groups { get; set; }
    }

    public partial class NotesGroup
    {
        [JsonProperty("notes", NullValueHandling = NullValueHandling.Ignore)]
        public List<string> Notes { get; set; }
    }

    public partial class Scorecard
    {
        [JsonProperty("innings", NullValueHandling = NullValueHandling.Ignore)]
        public List<Inning> Innings { get; set; }
    }

    public partial class Inning
    {
        [JsonProperty("inningNumber", NullValueHandling = NullValueHandling.Ignore)]
        public long? InningNumber { get; set; }

        [JsonProperty("team", NullValueHandling = NullValueHandling.Ignore)]
        public TeamInfoClass Team { get; set; }

        [JsonProperty("isBatted", NullValueHandling = NullValueHandling.Ignore)]
        public bool? IsBatted { get; set; }

        [JsonProperty("runs", NullValueHandling = NullValueHandling.Ignore)]
        public long? Runs { get; set; }

        [JsonProperty("wickets", NullValueHandling = NullValueHandling.Ignore)]
        public long? Wickets { get; set; }

        [JsonProperty("lead", NullValueHandling = NullValueHandling.Ignore)]
        public long? Lead { get; set; }

        [JsonProperty("target", NullValueHandling = NullValueHandling.Ignore)]
        public long? Target { get; set; }

        [JsonProperty("overs", NullValueHandling = NullValueHandling.Ignore)]
        public double? Overs { get; set; }

        [JsonProperty("balls", NullValueHandling = NullValueHandling.Ignore)]
        public long? Balls { get; set; }

        [JsonProperty("totalOvers", NullValueHandling = NullValueHandling.Ignore)]
        public long? TotalOvers { get; set; }

        [JsonProperty("totalBalls", NullValueHandling = NullValueHandling.Ignore)]
        public long? TotalBalls { get; set; }

        [JsonProperty("minutes")]
        public object Minutes { get; set; }

        [JsonProperty("extras", NullValueHandling = NullValueHandling.Ignore)]
        public long? Extras { get; set; }

        [JsonProperty("byes", NullValueHandling = NullValueHandling.Ignore)]
        public long? Byes { get; set; }

        [JsonProperty("legbyes", NullValueHandling = NullValueHandling.Ignore)]
        public long? Legbyes { get; set; }

        [JsonProperty("wides", NullValueHandling = NullValueHandling.Ignore)]
        public long? Wides { get; set; }

        [JsonProperty("noballs", NullValueHandling = NullValueHandling.Ignore)]
        public long? Noballs { get; set; }

        [JsonProperty("penalties", NullValueHandling = NullValueHandling.Ignore)]
        public long? Penalties { get; set; }

        [JsonProperty("event", NullValueHandling = NullValueHandling.Ignore)]
        public long? Event { get; set; }

        [JsonProperty("ballsPerOver", NullValueHandling = NullValueHandling.Ignore)]
        public long? BallsPerOver { get; set; }

        [JsonProperty("inningBatsmen", NullValueHandling = NullValueHandling.Ignore)]
        public List<InningBatsmanElement> InningBatsmen { get; set; }

        [JsonProperty("inningBowlers", NullValueHandling = NullValueHandling.Ignore)]
        public List<InningBowler> InningBowlers { get; set; }

        [JsonProperty("inningPartnerships", NullValueHandling = NullValueHandling.Ignore)]
        public List<InningPartnership> InningPartnerships { get; set; }

        [JsonProperty("inningOvers", NullValueHandling = NullValueHandling.Ignore)]
        public List<InningOver> InningOvers { get; set; }

        [JsonProperty("inningWickets", NullValueHandling = NullValueHandling.Ignore)]
        public List<InningBatsmanElement> InningWickets { get; set; }

        [JsonProperty("inningFallOfWickets", NullValueHandling = NullValueHandling.Ignore)]
        public List<InningFallOfWicket> InningFallOfWickets { get; set; }
    }

    public partial class InningBatsmanElement
    {
        [JsonProperty("playerRoleType", NullValueHandling = NullValueHandling.Ignore)]
        public string PlayerRoleType { get; set; }

        [JsonProperty("player", NullValueHandling = NullValueHandling.Ignore)]
        public Player1Class Player { get; set; }

        [JsonProperty("battedType", NullValueHandling = NullValueHandling.Ignore)]
        public string BattedType { get; set; }

        [JsonProperty("runs")]
        public long? Runs { get; set; }

        [JsonProperty("balls")]
        public long? Balls { get; set; }

        [JsonProperty("minutes")]
        public long? Minutes { get; set; }

        [JsonProperty("fours")]
        public long? Fours { get; set; }

        [JsonProperty("sixes")]
        public long? Sixes { get; set; }

        [JsonProperty("strikerate")]
        public double? Strikerate { get; set; }

        [JsonProperty("isOut", NullValueHandling = NullValueHandling.Ignore)]
        public bool? IsOut { get; set; }

        [JsonProperty("dismissalType")]
        public long? DismissalType { get; set; }

        [JsonProperty("dismissalBatsman")]
        public Player1Class DismissalBatsman { get; set; }

        [JsonProperty("dismissalBowler")]
        public Player1Class DismissalBowler { get; set; }

        [JsonProperty("dismissalFielders")]
        public List<DismissalFielder> DismissalFielders { get; set; }

        [JsonProperty("dismissalText")]
        public DismissalText DismissalText { get; set; }

        [JsonProperty("dismissalComment")]
        public List<DismissalComment> DismissalComment { get; set; }

        [JsonProperty("fowOrder")]
        public long? FowOrder { get; set; }

        [JsonProperty("fowWicketNum")]
        public long? FowWicketNum { get; set; }

        [JsonProperty("fowRuns")]
        public long? FowRuns { get; set; }

        [JsonProperty("fowBalls")]
        public object FowBalls { get; set; }

        [JsonProperty("fowOvers")]
        public double? FowOvers { get; set; }

        [JsonProperty("fowOverNumber")]
        public long? FowOverNumber { get; set; }

        [JsonProperty("ballOversActual")]
        public double? BallOversActual { get; set; }

        [JsonProperty("ballOversUnique")]
        public double? BallOversUnique { get; set; }

        [JsonProperty("ballTotalRuns")]
        public long? BallTotalRuns { get; set; }

        [JsonProperty("ballBatsmanRuns")]
        public long? BallBatsmanRuns { get; set; }

        [JsonProperty("videos", NullValueHandling = NullValueHandling.Ignore)]
        public List<Video> Videos { get; set; }

        [JsonProperty("images", NullValueHandling = NullValueHandling.Ignore)]
        public List<Image> Images { get; set; }

        [JsonProperty("currentType")]
        public long? CurrentType { get; set; }
    }

    public partial class DismissalComment
    {
        [JsonProperty("type", NullValueHandling = NullValueHandling.Ignore)]
        public string Type { get; set; }

        [JsonProperty("html", NullValueHandling = NullValueHandling.Ignore)]
        public string Html { get; set; }
    }

    public partial class DismissalFielder
    {
        [JsonProperty("player")]
        public Player1Class Player { get; set; }

        [JsonProperty("isKeeper", NullValueHandling = NullValueHandling.Ignore)]
        public long? IsKeeper { get; set; }

        [JsonProperty("isSubstitute", NullValueHandling = NullValueHandling.Ignore)]
        public long? IsSubstitute { get; set; }
    }

    public partial class DismissalText
    {
        [JsonProperty("short", NullValueHandling = NullValueHandling.Ignore)]
        public string Short { get; set; }

        [JsonProperty("long", NullValueHandling = NullValueHandling.Ignore)]
        public string Long { get; set; }

        [JsonProperty("commentary", NullValueHandling = NullValueHandling.Ignore)]
        public string Commentary { get; set; }
    }

    public partial class Video
    {
        [JsonProperty("id", NullValueHandling = NullValueHandling.Ignore)]
        public long? Id { get; set; }

        [JsonProperty("objectId", NullValueHandling = NullValueHandling.Ignore)]
        public long? ObjectId { get; set; }

        [JsonProperty("scribeId", NullValueHandling = NullValueHandling.Ignore)]
        public long? ScribeId { get; set; }

        [JsonProperty("statusTypeId", NullValueHandling = NullValueHandling.Ignore)]
        public long? StatusTypeId { get; set; }

        [JsonProperty("slug", NullValueHandling = NullValueHandling.Ignore)]
        public string Slug { get; set; }

        [JsonProperty("title", NullValueHandling = NullValueHandling.Ignore)]
        public string Title { get; set; }

        [JsonProperty("subTitle", NullValueHandling = NullValueHandling.Ignore)]
        public string SubTitle { get; set; }

        [JsonProperty("seoTitle", NullValueHandling = NullValueHandling.Ignore)]
        public string SeoTitle { get; set; }

        [JsonProperty("summary", NullValueHandling = NullValueHandling.Ignore)]
        public string Summary { get; set; }

        [JsonProperty("duration", NullValueHandling = NullValueHandling.Ignore)]
        public long? Duration { get; set; }

        [JsonProperty("genreId", NullValueHandling = NullValueHandling.Ignore)]
        public long? GenreId { get; set; }

        [JsonProperty("genreType", NullValueHandling = NullValueHandling.Ignore)]
        public string GenreType { get; set; }

        [JsonProperty("genreName", NullValueHandling = NullValueHandling.Ignore)]
        public string GenreName { get; set; }

        [JsonProperty("publishedAt", NullValueHandling = NullValueHandling.Ignore)]
        public DateTimeOffset? PublishedAt { get; set; }

        [JsonProperty("modifiedAt", NullValueHandling = NullValueHandling.Ignore)]
        public DateTimeOffset? ModifiedAt { get; set; }

        [JsonProperty("recordedAt", NullValueHandling = NullValueHandling.Ignore)]
        public DateTimeOffset? RecordedAt { get; set; }

        [JsonProperty("expireAt", NullValueHandling = NullValueHandling.Ignore)]
        public DateTimeOffset? ExpireAt { get; set; }

        [JsonProperty("countryCodes", NullValueHandling = NullValueHandling.Ignore)]
        public List<string> CountryCodes { get; set; }

        [JsonProperty("imageUrl", NullValueHandling = NullValueHandling.Ignore)]
        public string ImageUrl { get; set; }

        [JsonProperty("origin", NullValueHandling = NullValueHandling.Ignore)]
        public Origin Origin { get; set; }

        [JsonProperty("assetId", NullValueHandling = NullValueHandling.Ignore)]
        public string AssetId { get; set; }

        [JsonProperty("scribeMongoId", NullValueHandling = NullValueHandling.Ignore)]
        public string ScribeMongoId { get; set; }

        [JsonProperty("language", NullValueHandling = NullValueHandling.Ignore)]
        public string Language { get; set; }
    }

    public partial class Origin
    {
        [JsonProperty("type", NullValueHandling = NullValueHandling.Ignore)]
        public string Type { get; set; }
    }

    public partial class InningBowler
    {
        [JsonProperty("player", NullValueHandling = NullValueHandling.Ignore)]
        public Player1Class Player { get; set; }

        [JsonProperty("bowledType", NullValueHandling = NullValueHandling.Ignore)]
        public string BowledType { get; set; }

        [JsonProperty("overs", NullValueHandling = NullValueHandling.Ignore)]
        public double? Overs { get; set; }

        [JsonProperty("balls", NullValueHandling = NullValueHandling.Ignore)]
        public long? Balls { get; set; }

        [JsonProperty("maidens", NullValueHandling = NullValueHandling.Ignore)]
        public long? Maidens { get; set; }

        [JsonProperty("conceded", NullValueHandling = NullValueHandling.Ignore)]
        public long? Conceded { get; set; }

        [JsonProperty("wickets", NullValueHandling = NullValueHandling.Ignore)]
        public long? Wickets { get; set; }

        [JsonProperty("economy", NullValueHandling = NullValueHandling.Ignore)]
        public double? Economy { get; set; }

        [JsonProperty("runsPerBall", NullValueHandling = NullValueHandling.Ignore)]
        public double? RunsPerBall { get; set; }

        [JsonProperty("dots", NullValueHandling = NullValueHandling.Ignore)]
        public long? Dots { get; set; }

        [JsonProperty("fours", NullValueHandling = NullValueHandling.Ignore)]
        public long? Fours { get; set; }

        [JsonProperty("sixes", NullValueHandling = NullValueHandling.Ignore)]
        public long? Sixes { get; set; }

        [JsonProperty("wides", NullValueHandling = NullValueHandling.Ignore)]
        public long? Wides { get; set; }

        [JsonProperty("noballs", NullValueHandling = NullValueHandling.Ignore)]
        public long? Noballs { get; set; }

        [JsonProperty("videos", NullValueHandling = NullValueHandling.Ignore)]
        public List<Video> Videos { get; set; }

        [JsonProperty("images", NullValueHandling = NullValueHandling.Ignore)]
        public List<Image> Images { get; set; }

        [JsonProperty("currentType")]
        public long? CurrentType { get; set; }

        [JsonProperty("inningWickets", NullValueHandling = NullValueHandling.Ignore)]
        public List<InningWicket> InningWickets { get; set; }
    }

    public partial class InningWicket
    {
        [JsonProperty("dismissalBatsman", NullValueHandling = NullValueHandling.Ignore)]
        public Player1Class DismissalBatsman { get; set; }

        [JsonProperty("dismissalType", NullValueHandling = NullValueHandling.Ignore)]
        public long? DismissalType { get; set; }

        [JsonProperty("dismissalComment", NullValueHandling = NullValueHandling.Ignore)]
        public List<DismissalComment> DismissalComment { get; set; }

        [JsonProperty("fowOrder", NullValueHandling = NullValueHandling.Ignore)]
        public long? FowOrder { get; set; }

        [JsonProperty("fowWicketNum", NullValueHandling = NullValueHandling.Ignore)]
        public long? FowWicketNum { get; set; }

        [JsonProperty("fowRuns", NullValueHandling = NullValueHandling.Ignore)]
        public long? FowRuns { get; set; }

        [JsonProperty("fowOvers", NullValueHandling = NullValueHandling.Ignore)]
        public double? FowOvers { get; set; }

        [JsonProperty("fowBalls")]
        public object FowBalls { get; set; }
    }

    public partial class InningFallOfWicket
    {
        [JsonProperty("fowOrder", NullValueHandling = NullValueHandling.Ignore)]
        public long? FowOrder { get; set; }

        [JsonProperty("fowWicketNum", NullValueHandling = NullValueHandling.Ignore)]
        public long? FowWicketNum { get; set; }

        [JsonProperty("fowRuns", NullValueHandling = NullValueHandling.Ignore)]
        public long? FowRuns { get; set; }

        [JsonProperty("fowOvers", NullValueHandling = NullValueHandling.Ignore)]
        public double? FowOvers { get; set; }

        [JsonProperty("fowBalls")]
        public object FowBalls { get; set; }
    }

    public partial class InningOver
    {
        [JsonProperty("overNumber", NullValueHandling = NullValueHandling.Ignore)]
        public long? OverNumber { get; set; }

        [JsonProperty("overRuns", NullValueHandling = NullValueHandling.Ignore)]
        public long? OverRuns { get; set; }

        [JsonProperty("overWickets", NullValueHandling = NullValueHandling.Ignore)]
        public long? OverWickets { get; set; }

        [JsonProperty("isComplete", NullValueHandling = NullValueHandling.Ignore)]
        public bool? IsComplete { get; set; }

        [JsonProperty("totalBalls", NullValueHandling = NullValueHandling.Ignore)]
        public long? TotalBalls { get; set; }

        [JsonProperty("totalRuns", NullValueHandling = NullValueHandling.Ignore)]
        public long? TotalRuns { get; set; }

        [JsonProperty("totalWickets", NullValueHandling = NullValueHandling.Ignore)]
        public long? TotalWickets { get; set; }

        [JsonProperty("overRunRate", NullValueHandling = NullValueHandling.Ignore)]
        public double? OverRunRate { get; set; }

        [JsonProperty("bowlers", NullValueHandling = NullValueHandling.Ignore)]
        public List<Bowler> Bowlers { get; set; }

        [JsonProperty("requiredRunRate", NullValueHandling = NullValueHandling.Ignore)]
        public double? RequiredRunRate { get; set; }

        [JsonProperty("requiredRuns", NullValueHandling = NullValueHandling.Ignore)]
        public long? RequiredRuns { get; set; }

        [JsonProperty("remainingBalls", NullValueHandling = NullValueHandling.Ignore)]
        public long? RemainingBalls { get; set; }

        [JsonProperty("balls", NullValueHandling = NullValueHandling.Ignore)]
        public List<Ball> Balls { get; set; }
    }

    public partial class Ball
    {
        [JsonProperty("_uid", NullValueHandling = NullValueHandling.Ignore)]
        public long? Uid { get; set; }

        [JsonProperty("id", NullValueHandling = NullValueHandling.Ignore)]
        public long? Id { get; set; }

        [JsonProperty("inningNumber", NullValueHandling = NullValueHandling.Ignore)]
        public long? InningNumber { get; set; }

        [JsonProperty("ballsActual")]
        public object BallsActual { get; set; }

        [JsonProperty("ballsUnique")]
        public object BallsUnique { get; set; }

        [JsonProperty("oversUnique", NullValueHandling = NullValueHandling.Ignore)]
        public double? OversUnique { get; set; }

        [JsonProperty("oversActual", NullValueHandling = NullValueHandling.Ignore)]
        public double? OversActual { get; set; }

        [JsonProperty("overNumber", NullValueHandling = NullValueHandling.Ignore)]
        public long? OverNumber { get; set; }

        [JsonProperty("ballNumber", NullValueHandling = NullValueHandling.Ignore)]
        public long? BallNumber { get; set; }

        [JsonProperty("totalRuns", NullValueHandling = NullValueHandling.Ignore)]
        public long? TotalRuns { get; set; }

        [JsonProperty("batsmanRuns", NullValueHandling = NullValueHandling.Ignore)]
        public long? BatsmanRuns { get; set; }

        [JsonProperty("isFour", NullValueHandling = NullValueHandling.Ignore)]
        public bool? IsFour { get; set; }

        [JsonProperty("isSix", NullValueHandling = NullValueHandling.Ignore)]
        public bool? IsSix { get; set; }

        [JsonProperty("isWicket", NullValueHandling = NullValueHandling.Ignore)]
        public bool? IsWicket { get; set; }

        [JsonProperty("dismissalType")]
        public long? DismissalType { get; set; }

        [JsonProperty("byes", NullValueHandling = NullValueHandling.Ignore)]
        public long? Byes { get; set; }

        [JsonProperty("legbyes", NullValueHandling = NullValueHandling.Ignore)]
        public long? Legbyes { get; set; }

        [JsonProperty("wides", NullValueHandling = NullValueHandling.Ignore)]
        public long? Wides { get; set; }

        [JsonProperty("noballs", NullValueHandling = NullValueHandling.Ignore)]
        public long? Noballs { get; set; }

        [JsonProperty("timestamp", NullValueHandling = NullValueHandling.Ignore)]
        public DateTimeOffset? Timestamp { get; set; }

        [JsonProperty("batsmanPlayerId", NullValueHandling = NullValueHandling.Ignore)]
        public long? BatsmanPlayerId { get; set; }

        [JsonProperty("bowlerPlayerId", NullValueHandling = NullValueHandling.Ignore)]
        public long? BowlerPlayerId { get; set; }

        [JsonProperty("totalInningRuns", NullValueHandling = NullValueHandling.Ignore)]
        public long? TotalInningRuns { get; set; }
    }

    public partial class Bowler
    {
        [JsonProperty("id", NullValueHandling = NullValueHandling.Ignore)]
        public long? Id { get; set; }

        [JsonProperty("objectId", NullValueHandling = NullValueHandling.Ignore)]
        public long? ObjectId { get; set; }

        [JsonProperty("name", NullValueHandling = NullValueHandling.Ignore)]
        public string Name { get; set; }

        [JsonProperty("longName", NullValueHandling = NullValueHandling.Ignore)]
        public string LongName { get; set; }

        [JsonProperty("mobileName", NullValueHandling = NullValueHandling.Ignore)]
        public string MobileName { get; set; }

        [JsonProperty("indexName", NullValueHandling = NullValueHandling.Ignore)]
        public string IndexName { get; set; }

        [JsonProperty("slug", NullValueHandling = NullValueHandling.Ignore)]
        public string Slug { get; set; }
    }

    public partial class InningPartnership
    {
        [JsonProperty("player1", NullValueHandling = NullValueHandling.Ignore)]
        public Player1Class Player1 { get; set; }

        [JsonProperty("player2", NullValueHandling = NullValueHandling.Ignore)]
        public Player1Class Player2 { get; set; }

        [JsonProperty("outPlayerId")]
        public long? OutPlayerId { get; set; }

        [JsonProperty("player1Runs", NullValueHandling = NullValueHandling.Ignore)]
        public long? Player1Runs { get; set; }

        [JsonProperty("player1Balls", NullValueHandling = NullValueHandling.Ignore)]
        public long? Player1Balls { get; set; }

        [JsonProperty("player2Runs", NullValueHandling = NullValueHandling.Ignore)]
        public long? Player2Runs { get; set; }

        [JsonProperty("player2Balls", NullValueHandling = NullValueHandling.Ignore)]
        public long? Player2Balls { get; set; }

        [JsonProperty("runs", NullValueHandling = NullValueHandling.Ignore)]
        public long? Runs { get; set; }

        [JsonProperty("balls", NullValueHandling = NullValueHandling.Ignore)]
        public long? Balls { get; set; }

        [JsonProperty("overs", NullValueHandling = NullValueHandling.Ignore)]
        public double? Overs { get; set; }

        [JsonProperty("isLive", NullValueHandling = NullValueHandling.Ignore)]
        public bool? IsLive { get; set; }
    }

    public partial class SupportInfo
    {
        [JsonProperty("stories", NullValueHandling = NullValueHandling.Ignore)]
        public List<Story> Stories { get; set; }

        [JsonProperty("videos", NullValueHandling = NullValueHandling.Ignore)]
        public List<Video> Videos { get; set; }

        [JsonProperty("seriesStories", NullValueHandling = NullValueHandling.Ignore)]
        public List<object> SeriesStories { get; set; }

        [JsonProperty("inning", NullValueHandling = NullValueHandling.Ignore)]
        public Inning Inning { get; set; }

        [JsonProperty("seriesStandings", NullValueHandling = NullValueHandling.Ignore)]
        public SeriesStandings SeriesStandings { get; set; }

        [JsonProperty("liveInfo", NullValueHandling = NullValueHandling.Ignore)]
        public LiveInfo LiveInfo { get; set; }

        [JsonProperty("liveSummary", NullValueHandling = NullValueHandling.Ignore)]
        public LiveSummary LiveSummary { get; set; }

        [JsonProperty("mostValuedPlayerOfTheMatch", NullValueHandling = NullValueHandling.Ignore)]
        public MostValuedPlayerOfTheMatch MostValuedPlayerOfTheMatch { get; set; }

        [JsonProperty("playersOfTheMatch", NullValueHandling = NullValueHandling.Ignore)]
        public List<PlayersOfTheMatch> PlayersOfTheMatch { get; set; }

        [JsonProperty("playersOfTheSeries")]
        public object PlayersOfTheSeries { get; set; }

        [JsonProperty("matchSeriesResult")]
        public object MatchSeriesResult { get; set; }

        [JsonProperty("recentReportStory", NullValueHandling = NullValueHandling.Ignore)]
        public Story RecentReportStory { get; set; }

        [JsonProperty("recentPreviewStory", NullValueHandling = NullValueHandling.Ignore)]
        public Story RecentPreviewStory { get; set; }

        [JsonProperty("liveBlogStory", NullValueHandling = NullValueHandling.Ignore)]
        public Story LiveBlogStory { get; set; }

        [JsonProperty("fantasyPickStory", NullValueHandling = NullValueHandling.Ignore)]
        public Story FantasyPickStory { get; set; }

        [JsonProperty("commentaryStarted", NullValueHandling = NullValueHandling.Ignore)]
        public bool? CommentaryStarted { get; set; }

        [JsonProperty("superOver", NullValueHandling = NullValueHandling.Ignore)]
        public bool? SuperOver { get; set; }

        [JsonProperty("bet365Odds")]
        public object Bet365Odds { get; set; }

        [JsonProperty("hasTeamPlayers", NullValueHandling = NullValueHandling.Ignore)]
        public bool? HasTeamPlayers { get; set; }

        [JsonProperty("hasTeamSquads", NullValueHandling = NullValueHandling.Ignore)]
        public bool? HasTeamSquads { get; set; }

        [JsonProperty("hasStats", NullValueHandling = NullValueHandling.Ignore)]
        public bool? HasStats { get; set; }
    }

    public partial class Story
    {
        [JsonProperty("id", NullValueHandling = NullValueHandling.Ignore)]
        public long? Id { get; set; }

        [JsonProperty("objectId", NullValueHandling = NullValueHandling.Ignore)]
        public long? ObjectId { get; set; }

        [JsonProperty("scribeId", NullValueHandling = NullValueHandling.Ignore)]
        public long? ScribeId { get; set; }

        [JsonProperty("slug", NullValueHandling = NullValueHandling.Ignore)]
        public string Slug { get; set; }

        [JsonProperty("title", NullValueHandling = NullValueHandling.Ignore)]
        public string Title { get; set; }

        [JsonProperty("subTitle", NullValueHandling = NullValueHandling.Ignore)]
        public string SubTitle { get; set; }

        [JsonProperty("seoTitle", NullValueHandling = NullValueHandling.Ignore)]
        public string SeoTitle { get; set; }

        [JsonProperty("summary", NullValueHandling = NullValueHandling.Ignore)]
        public string Summary { get; set; }

        [JsonProperty("byline", NullValueHandling = NullValueHandling.Ignore)]
        public string Byline { get; set; }

        [JsonProperty("categoryType", NullValueHandling = NullValueHandling.Ignore)]
        public string CategoryType { get; set; }

        [JsonProperty("genreType", NullValueHandling = NullValueHandling.Ignore)]
        public string GenreType { get; set; }

        [JsonProperty("genreId", NullValueHandling = NullValueHandling.Ignore)]
        public long? GenreId { get; set; }

        [JsonProperty("genreName", NullValueHandling = NullValueHandling.Ignore)]
        public string GenreName { get; set; }

        [JsonProperty("publishedAt", NullValueHandling = NullValueHandling.Ignore)]
        public DateTimeOffset? PublishedAt { get; set; }

        [JsonProperty("modifiedAt", NullValueHandling = NullValueHandling.Ignore)]
        public DateTimeOffset? ModifiedAt { get; set; }

        [JsonProperty("showPublishedAt", NullValueHandling = NullValueHandling.Ignore)]
        public bool? ShowPublishedAt { get; set; }

        [JsonProperty("showModifiedAt", NullValueHandling = NullValueHandling.Ignore)]
        public bool? ShowModifiedAt { get; set; }

        [JsonProperty("day")]
        public object Day { get; set; }

        [JsonProperty("authorId")]
        public long? AuthorId { get; set; }

        [JsonProperty("image", NullValueHandling = NullValueHandling.Ignore)]
        public Image Image { get; set; }

        [JsonProperty("matchMeta")]
        public MatchMeta MatchMeta { get; set; }

        [JsonProperty("isLiveBlog", NullValueHandling = NullValueHandling.Ignore)]
        public bool? IsLiveBlog { get; set; }

        [JsonProperty("isLive", NullValueHandling = NullValueHandling.Ignore)]
        public bool? IsLive { get; set; }

        [JsonProperty("language", NullValueHandling = NullValueHandling.Ignore)]
        public string Language { get; set; }

        [JsonProperty("generatedAt", NullValueHandling = NullValueHandling.Ignore)]
        public DateTimeOffset? GeneratedAt { get; set; }

        [JsonProperty("credit", NullValueHandling = NullValueHandling.Ignore)]
        public string Credit { get; set; }

        [JsonProperty("copyright", NullValueHandling = NullValueHandling.Ignore)]
        public string Copyright { get; set; }

        [JsonProperty("overviewHtml")]
        public string OverviewHtml { get; set; }

        [JsonProperty("keywords", NullValueHandling = NullValueHandling.Ignore)]
        public List<string> Keywords { get; set; }

        [JsonProperty("video")]
        public Video Video { get; set; }

        [JsonProperty("storyGenre", NullValueHandling = NullValueHandling.Ignore)]
        public StoryGenre StoryGenre { get; set; }
    }

    public partial class MatchMeta
    {
        [JsonProperty("id", NullValueHandling = NullValueHandling.Ignore)]
        public long? Id { get; set; }

        [JsonProperty("objectId", NullValueHandling = NullValueHandling.Ignore)]
        public long? ObjectId { get; set; }

        [JsonProperty("scribeId", NullValueHandling = NullValueHandling.Ignore)]
        public long? ScribeId { get; set; }

        [JsonProperty("slug", NullValueHandling = NullValueHandling.Ignore)]
        public string Slug { get; set; }

        [JsonProperty("seriesId", NullValueHandling = NullValueHandling.Ignore)]
        public long? SeriesId { get; set; }

        [JsonProperty("seriesObjectId", NullValueHandling = NullValueHandling.Ignore)]
        public long? SeriesObjectId { get; set; }

        [JsonProperty("seriesSlug", NullValueHandling = NullValueHandling.Ignore)]
        public string SeriesSlug { get; set; }
    }

    public partial class StoryGenre
    {
        [JsonProperty("id", NullValueHandling = NullValueHandling.Ignore)]
        public long? Id { get; set; }

        [JsonProperty("slug", NullValueHandling = NullValueHandling.Ignore)]
        public string Slug { get; set; }

        [JsonProperty("title", NullValueHandling = NullValueHandling.Ignore)]
        public string Title { get; set; }

        [JsonProperty("summary")]
        public object Summary { get; set; }
    }

    public partial class MostValuedPlayerOfTheMatch
    {
        [JsonProperty("player", NullValueHandling = NullValueHandling.Ignore)]
        public Player1Class Player { get; set; }

        [JsonProperty("team", NullValueHandling = NullValueHandling.Ignore)]
        public TeamInfoClass Team { get; set; }

        [JsonProperty("runs", NullValueHandling = NullValueHandling.Ignore)]
        public long? Runs { get; set; }

        [JsonProperty("ballsFaced", NullValueHandling = NullValueHandling.Ignore)]
        public long? BallsFaced { get; set; }

        [JsonProperty("smartRuns", NullValueHandling = NullValueHandling.Ignore)]
        public double? SmartRuns { get; set; }

        [JsonProperty("wickets", NullValueHandling = NullValueHandling.Ignore)]
        public long? Wickets { get; set; }

        [JsonProperty("conceded", NullValueHandling = NullValueHandling.Ignore)]
        public long? Conceded { get; set; }

        [JsonProperty("smartWickets", NullValueHandling = NullValueHandling.Ignore)]
        public long? SmartWickets { get; set; }

        [JsonProperty("totalImpact", NullValueHandling = NullValueHandling.Ignore)]
        public double? TotalImpact { get; set; }
    }

    public partial class PlayersOfTheMatch
    {
        [JsonProperty("team", NullValueHandling = NullValueHandling.Ignore)]
        public TeamInfoClass Team { get; set; }

        [JsonProperty("player", NullValueHandling = NullValueHandling.Ignore)]
        public Player1Class Player { get; set; }

        [JsonProperty("inningStats", NullValueHandling = NullValueHandling.Ignore)]
        public List<InningStat> InningStats { get; set; }
    }

    public partial class InningStat
    {
        [JsonProperty("inningNumber", NullValueHandling = NullValueHandling.Ignore)]
        public long? InningNumber { get; set; }

        [JsonProperty("stat", NullValueHandling = NullValueHandling.Ignore)]
        public Stat Stat { get; set; }
    }

    public partial class Stat
    {
        [JsonProperty("runs")]
        public long? Runs { get; set; }

        [JsonProperty("ballsFaced")]
        public long? BallsFaced { get; set; }

        [JsonProperty("notouts")]
        public long? Notouts { get; set; }

        [JsonProperty("wickets")]
        public object Wickets { get; set; }

        [JsonProperty("conceded")]
        public object Conceded { get; set; }

        [JsonProperty("balls")]
        public object Balls { get; set; }

        [JsonProperty("economy")]
        public object Economy { get; set; }

        [JsonProperty("caught")]
        public long? Caught { get; set; }

        [JsonProperty("stumped")]
        public long? Stumped { get; set; }
    }

    public partial class SeriesStandings
    {
        [JsonProperty("notes", NullValueHandling = NullValueHandling.Ignore)]
        public string Notes { get; set; }

        [JsonProperty("teamsToQualifyCount", NullValueHandling = NullValueHandling.Ignore)]
        public long? TeamsToQualifyCount { get; set; }

        [JsonProperty("available", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, bool> Available { get; set; }

        [JsonProperty("groups", NullValueHandling = NullValueHandling.Ignore)]
        public List<SeriesStandingsGroup> Groups { get; set; }
    }

    public partial class SeriesStandingsGroup
    {
        [JsonProperty("name", NullValueHandling = NullValueHandling.Ignore)]
        public string Name { get; set; }

        [JsonProperty("teamStats", NullValueHandling = NullValueHandling.Ignore)]
        public List<TeamStat> TeamStats { get; set; }
    }

    public partial class TeamStat
    {
        [JsonProperty("teamInfo", NullValueHandling = NullValueHandling.Ignore)]
        public TeamInfoClass TeamInfo { get; set; }

        [JsonProperty("rank", NullValueHandling = NullValueHandling.Ignore)]
        public long? Rank { get; set; }

        [JsonProperty("seriesPlayed")]
        public object SeriesPlayed { get; set; }

        [JsonProperty("seriesWon")]
        public object SeriesWon { get; set; }

        [JsonProperty("matchesPlayed", NullValueHandling = NullValueHandling.Ignore)]
        public string MatchesPlayed { get; set; }

        [JsonProperty("matchesWon", NullValueHandling = NullValueHandling.Ignore)]
        public long? MatchesWon { get; set; }

        [JsonProperty("matchesLost", NullValueHandling = NullValueHandling.Ignore)]
        public long? MatchesLost { get; set; }

        [JsonProperty("matchesTied", NullValueHandling = NullValueHandling.Ignore)]
        public long? MatchesTied { get; set; }

        [JsonProperty("matchesDrawn", NullValueHandling = NullValueHandling.Ignore)]
        public long? MatchesDrawn { get; set; }

        [JsonProperty("matchesNoResult", NullValueHandling = NullValueHandling.Ignore)]
        public long? MatchesNoResult { get; set; }

        [JsonProperty("points", NullValueHandling = NullValueHandling.Ignore)]
        public long? Points { get; set; }

        [JsonProperty("quotient")]
        public object Quotient { get; set; }

        [JsonProperty("nrr", NullValueHandling = NullValueHandling.Ignore)]
        public double? Nrr { get; set; }

        [JsonProperty("rpwr")]
        public object Rpwr { get; set; }

        [JsonProperty("for", NullValueHandling = NullValueHandling.Ignore)]
        public string For { get; set; }

        [JsonProperty("against", NullValueHandling = NullValueHandling.Ignore)]
        public string Against { get; set; }

        [JsonProperty("pct")]
        public object Pct { get; set; }

        [JsonProperty("recentMatches", NullValueHandling = NullValueHandling.Ignore)]
        public List<RecentMatch> RecentMatches { get; set; }
    }

    public partial class RecentMatch
    {
        [JsonProperty("_uid", NullValueHandling = NullValueHandling.Ignore)]
        public long? Uid { get; set; }

        [JsonProperty("id", NullValueHandling = NullValueHandling.Ignore)]
        public long? Id { get; set; }

        [JsonProperty("objectId", NullValueHandling = NullValueHandling.Ignore)]
        public long? ObjectId { get; set; }

        [JsonProperty("slug", NullValueHandling = NullValueHandling.Ignore)]
        public string Slug { get; set; }

        [JsonProperty("title", NullValueHandling = NullValueHandling.Ignore)]
        public string Title { get; set; }

        [JsonProperty("startDate", NullValueHandling = NullValueHandling.Ignore)]
        public DateTimeOffset? StartDate { get; set; }

        [JsonProperty("isCancelled", NullValueHandling = NullValueHandling.Ignore)]
        public bool? IsCancelled { get; set; }

        [JsonProperty("stage", NullValueHandling = NullValueHandling.Ignore)]
        public string Stage { get; set; }

        [JsonProperty("state", NullValueHandling = NullValueHandling.Ignore)]
        public string State { get; set; }

        [JsonProperty("winnerTeamId", NullValueHandling = NullValueHandling.Ignore)]
        public long? WinnerTeamId { get; set; }

        [JsonProperty("status", NullValueHandling = NullValueHandling.Ignore)]
        public string Status { get; set; }

        [JsonProperty("statusText", NullValueHandling = NullValueHandling.Ignore)]
        public string StatusText { get; set; }

        [JsonProperty("statusEng", NullValueHandling = NullValueHandling.Ignore)]
        public string StatusEng { get; set; }

        [JsonProperty("resultStatus", NullValueHandling = NullValueHandling.Ignore)]
        public long? ResultStatus { get; set; }

        [JsonProperty("series", NullValueHandling = NullValueHandling.Ignore)]
        public RecentMatchSeries Series { get; set; }

        [JsonProperty("ground", NullValueHandling = NullValueHandling.Ignore)]
        public Ground Ground { get; set; }

        [JsonProperty("teams", NullValueHandling = NullValueHandling.Ignore)]
        public List<RecentMatchTeam> Teams { get; set; }

        [JsonProperty("languages", NullValueHandling = NullValueHandling.Ignore)]
        public List<string> Languages { get; set; }
    }

    public partial class RecentMatchSeries
    {
        [JsonProperty("id", NullValueHandling = NullValueHandling.Ignore)]
        public long? Id { get; set; }

        [JsonProperty("objectId", NullValueHandling = NullValueHandling.Ignore)]
        public long? ObjectId { get; set; }

        [JsonProperty("slug", NullValueHandling = NullValueHandling.Ignore)]
        public string Slug { get; set; }

        [JsonProperty("name", NullValueHandling = NullValueHandling.Ignore)]
        public string Name { get; set; }

        [JsonProperty("longName", NullValueHandling = NullValueHandling.Ignore)]
        public string LongName { get; set; }

        [JsonProperty("alternateName", NullValueHandling = NullValueHandling.Ignore)]
        public string AlternateName { get; set; }

        [JsonProperty("longAlternateName", NullValueHandling = NullValueHandling.Ignore)]
        public string LongAlternateName { get; set; }

        [JsonProperty("season", NullValueHandling = NullValueHandling.Ignore)]
        public long? Season { get; set; }
    }

    public partial class RecentMatchTeam
    {
        [JsonProperty("team", NullValueHandling = NullValueHandling.Ignore)]
        public PurpleTeam Team { get; set; }

        [JsonProperty("score", NullValueHandling = NullValueHandling.Ignore)]
        public string Score { get; set; }

        [JsonProperty("scoreInfo")]
        public string ScoreInfo { get; set; }
    }

    public partial class PurpleTeam
    {
        [JsonProperty("id", NullValueHandling = NullValueHandling.Ignore)]
        public long? Id { get; set; }

        [JsonProperty("objectId", NullValueHandling = NullValueHandling.Ignore)]
        public long? ObjectId { get; set; }

        [JsonProperty("slug", NullValueHandling = NullValueHandling.Ignore)]
        public string Slug { get; set; }

        [JsonProperty("name", NullValueHandling = NullValueHandling.Ignore)]
        public string Name { get; set; }

        [JsonProperty("longName", NullValueHandling = NullValueHandling.Ignore)]
        public string LongName { get; set; }

        [JsonProperty("abbreviation", NullValueHandling = NullValueHandling.Ignore)]
        public string Abbreviation { get; set; }
    }

    public partial class MatchReferee
    {
        [JsonProperty("team", NullValueHandling = NullValueHandling.Ignore)]
        public TeamInfoClass Team { get; set; }

        [JsonProperty("player", NullValueHandling = NullValueHandling.Ignore)]
        public Player1Class Player { get; set; }
    }

    public partial class MatchSeries
    {
        [JsonProperty("id", NullValueHandling = NullValueHandling.Ignore)]
        public long? Id { get; set; }

        [JsonProperty("objectId", NullValueHandling = NullValueHandling.Ignore)]
        public long? ObjectId { get; set; }

        [JsonProperty("scribeId", NullValueHandling = NullValueHandling.Ignore)]
        public long? ScribeId { get; set; }

        [JsonProperty("slug", NullValueHandling = NullValueHandling.Ignore)]
        public string Slug { get; set; }

        [JsonProperty("name", NullValueHandling = NullValueHandling.Ignore)]
        public string Name { get; set; }

        [JsonProperty("longName", NullValueHandling = NullValueHandling.Ignore)]
        public string LongName { get; set; }

        [JsonProperty("alternateName", NullValueHandling = NullValueHandling.Ignore)]
        public string AlternateName { get; set; }

        [JsonProperty("longAlternateName", NullValueHandling = NullValueHandling.Ignore)]
        public string LongAlternateName { get; set; }

        [JsonProperty("year", NullValueHandling = NullValueHandling.Ignore)]
        public long? Year { get; set; }

        [JsonProperty("typeId", NullValueHandling = NullValueHandling.Ignore)]
        public long? TypeId { get; set; }

        [JsonProperty("isTrophy", NullValueHandling = NullValueHandling.Ignore)]
        public bool? IsTrophy { get; set; }

        [JsonProperty("description", NullValueHandling = NullValueHandling.Ignore)]
        public string Description { get; set; }

        [JsonProperty("season", NullValueHandling = NullValueHandling.Ignore)]
        public long? Season { get; set; }

        [JsonProperty("startDate", NullValueHandling = NullValueHandling.Ignore)]
        public DateTimeOffset? StartDate { get; set; }

        [JsonProperty("endDate", NullValueHandling = NullValueHandling.Ignore)]
        public DateTimeOffset? EndDate { get; set; }

        [JsonProperty("hasStandings", NullValueHandling = NullValueHandling.Ignore)]
        public bool? HasStandings { get; set; }

        [JsonProperty("totalVideos", NullValueHandling = NullValueHandling.Ignore)]
        public long? TotalVideos { get; set; }
    }

}