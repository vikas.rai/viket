using System;
using System.Collections.Generic;

using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
namespace Cricket.Business.Models
{
    public partial class StoryDetails
    {
        [JsonProperty("story", NullValueHandling = NullValueHandling.Ignore)]
        public Story Story { get; set; }

        [JsonProperty("content", NullValueHandling = NullValueHandling.Ignore)]
        public Content Content { get; set; }

        [JsonProperty("blogs", NullValueHandling = NullValueHandling.Ignore)]
        public List<object> Blogs { get; set; }

        [JsonProperty("author", NullValueHandling = NullValueHandling.Ignore)]
        public Author Author { get; set; }

        [JsonProperty("related", NullValueHandling = NullValueHandling.Ignore)]
        public Related Related { get; set; }
    }

    public partial class Author
    {
        [JsonProperty("id", NullValueHandling = NullValueHandling.Ignore)]
        public long? Id { get; set; }

        [JsonProperty("name", NullValueHandling = NullValueHandling.Ignore)]
        public string Name { get; set; }

        [JsonProperty("designation", NullValueHandling = NullValueHandling.Ignore)]
        public string Designation { get; set; }

        [JsonProperty("slug", NullValueHandling = NullValueHandling.Ignore)]
        public string Slug { get; set; }

        [JsonProperty("position", NullValueHandling = NullValueHandling.Ignore)]
        public string Position { get; set; }

        [JsonProperty("image", NullValueHandling = NullValueHandling.Ignore)]
        public Image Image { get; set; }

        [JsonProperty("twitterName")]
        public object TwitterName { get; set; }

        [JsonProperty("primaryLang", NullValueHandling = NullValueHandling.Ignore)]
        public string PrimaryLang { get; set; }
    }

    public partial class Content
    {
        [JsonProperty("items", NullValueHandling = NullValueHandling.Ignore)]
        public List<Item> Items { get; set; }
    }

    public partial class Item
    {
        [JsonProperty("type", NullValueHandling = NullValueHandling.Ignore)]
        public string Type { get; set; }

        [JsonProperty("video", NullValueHandling = NullValueHandling.Ignore)]
        public Video Video { get; set; }

        [JsonProperty("caption", NullValueHandling = NullValueHandling.Ignore)]
        public string Caption { get; set; }

        [JsonProperty("altVideo")]
        public object AltVideo { get; set; }

        [JsonProperty("altVideoCaption")]
        public object AltVideoCaption { get; set; }

        [JsonProperty("altImage")]
        public object AltImage { get; set; }

        [JsonProperty("preview", NullValueHandling = NullValueHandling.Ignore)]
        public bool? Preview { get; set; }

        [JsonProperty("html", NullValueHandling = NullValueHandling.Ignore)]
        public string Html { get; set; }

        [JsonProperty("title", NullValueHandling = NullValueHandling.Ignore)]
        public string Title { get; set; }

        [JsonProperty("bulletedText", NullValueHandling = NullValueHandling.Ignore)]
        public string BulletedText { get; set; }

        [JsonProperty("nonBulletedText", NullValueHandling = NullValueHandling.Ignore)]
        public string NonBulletedText { get; set; }

        [JsonProperty("image")]
        public Image Image { get; set; }

        [JsonProperty("displayWidth", NullValueHandling = NullValueHandling.Ignore)]
        public long? DisplayWidth { get; set; }

        [JsonProperty("captionPosition")]
        public object CaptionPosition { get; set; }
    }

    public partial class Related
    {
        [JsonProperty("stories", NullValueHandling = NullValueHandling.Ignore)]
        public List<Story> Stories { get; set; }

        [JsonProperty("videos", NullValueHandling = NullValueHandling.Ignore)]
        public List<object> Videos { get; set; }

        [JsonProperty("matches", NullValueHandling = NullValueHandling.Ignore)]
        public List<Match> Matches { get; set; }

        [JsonProperty("serieses", NullValueHandling = NullValueHandling.Ignore)]
        public List<SerieseElement> Serieses { get; set; }

        [JsonProperty("teams", NullValueHandling = NullValueHandling.Ignore)]
        public List<TeamInfoElement> Teams { get; set; }

        [JsonProperty("players", NullValueHandling = NullValueHandling.Ignore)]
        public List<object> Players { get; set; }

        [JsonProperty("grounds", NullValueHandling = NullValueHandling.Ignore)]
        public List<object> Grounds { get; set; }
    }

    public partial class SerieseElement
    {
        [JsonProperty("id", NullValueHandling = NullValueHandling.Ignore)]
        public long? Id { get; set; }

        [JsonProperty("objectId", NullValueHandling = NullValueHandling.Ignore)]
        public long? ObjectId { get; set; }

        [JsonProperty("scribeId", NullValueHandling = NullValueHandling.Ignore)]
        public long? ScribeId { get; set; }

        [JsonProperty("slug", NullValueHandling = NullValueHandling.Ignore)]
        public string Slug { get; set; }

        [JsonProperty("name", NullValueHandling = NullValueHandling.Ignore)]
        public string Name { get; set; }

        [JsonProperty("longName", NullValueHandling = NullValueHandling.Ignore)]
        public string LongName { get; set; }

        [JsonProperty("alternateName", NullValueHandling = NullValueHandling.Ignore)]
        public string AlternateName { get; set; }

        [JsonProperty("longAlternateName", NullValueHandling = NullValueHandling.Ignore)]
        public string LongAlternateName { get; set; }

        [JsonProperty("year", NullValueHandling = NullValueHandling.Ignore)]
        public long? Year { get; set; }

        [JsonProperty("typeId", NullValueHandling = NullValueHandling.Ignore)]
        public long? TypeId { get; set; }

        [JsonProperty("isTrophy", NullValueHandling = NullValueHandling.Ignore)]
        public bool? IsTrophy { get; set; }

        [JsonProperty("description", NullValueHandling = NullValueHandling.Ignore)]
        public string Description { get; set; }

        [JsonProperty("season", NullValueHandling = NullValueHandling.Ignore)]
        public long? Season { get; set; }

        [JsonProperty("startDate", NullValueHandling = NullValueHandling.Ignore)]
        public DateTimeOffset? StartDate { get; set; }

        [JsonProperty("endDate", NullValueHandling = NullValueHandling.Ignore)]
        public DateTimeOffset? EndDate { get; set; }

        [JsonProperty("hasStandings", NullValueHandling = NullValueHandling.Ignore)]
        public bool? HasStandings { get; set; }

        [JsonProperty("totalVideos", NullValueHandling = NullValueHandling.Ignore)]
        public long? TotalVideos { get; set; }
    }
    public partial class TeamInfoElement
    {
        [JsonProperty("id", NullValueHandling = NullValueHandling.Ignore)]
        public long? Id { get; set; }

        [JsonProperty("objectId", NullValueHandling = NullValueHandling.Ignore)]
        public long? ObjectId { get; set; }

        [JsonProperty("scribeId", NullValueHandling = NullValueHandling.Ignore)]
        public long? ScribeId { get; set; }

        [JsonProperty("slug", NullValueHandling = NullValueHandling.Ignore)]
        public string Slug { get; set; }

        [JsonProperty("name", NullValueHandling = NullValueHandling.Ignore)]
        public string Name { get; set; }

        [JsonProperty("longName", NullValueHandling = NullValueHandling.Ignore)]
        public string LongName { get; set; }

        [JsonProperty("abbreviation", NullValueHandling = NullValueHandling.Ignore)]
        public string Abbreviation { get; set; }

        [JsonProperty("isCountry", NullValueHandling = NullValueHandling.Ignore)]
        public bool? IsCountry { get; set; }

        [JsonProperty("primaryColor")]
        public string PrimaryColor { get; set; }

        [JsonProperty("image", NullValueHandling = NullValueHandling.Ignore)]
        public Image Image { get; set; }
    }
}
