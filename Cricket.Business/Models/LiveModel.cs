using System;
using System.Collections.Generic;

using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
namespace Cricket.Business.Models
{
    public partial class LiveMatchModel
    {
        [JsonProperty("match", NullValueHandling = NullValueHandling.Ignore)]
        public Match Match { get; set; }

        [JsonProperty("content", NullValueHandling = NullValueHandling.Ignore)]
        public Content Content { get; set; }
    }

    public partial class Content
    {
        [JsonProperty("recentBallCommentary", NullValueHandling = NullValueHandling.Ignore)]
        public RecentBallCommentary RecentBallCommentary { get; set; }

        [JsonProperty("teamRecentMatches", NullValueHandling = NullValueHandling.Ignore)]
        public object TeamRecentMatches { get; set; }

        [JsonProperty("headToHead")]
        public object HeadToHead { get; set; }

        [JsonProperty("winPoll")]
        public object WinPoll { get; set; }

        [JsonProperty("statsguruLinks", NullValueHandling = NullValueHandling.Ignore)]
        public List<object> StatsguruLinks { get; set; }

        [JsonProperty("livePerformance", NullValueHandling = NullValueHandling.Ignore)]
        public LivePerformance LivePerformance { get; set; }

        [JsonProperty("liveInningsPerformance", NullValueHandling = NullValueHandling.Ignore)]
        public LiveInningsPerformance LiveInningsPerformance { get; set; }

        [JsonProperty("scorecardSummary")]
        public object ScorecardSummary { get; set; }

        [JsonProperty("bestPerformance")]
        public object BestPerformance { get; set; }

        [JsonProperty("videos", NullValueHandling = NullValueHandling.Ignore)]
        public List<object> Videos { get; set; }
    }

    public partial class LiveInningsPerformance
    {
        [JsonProperty("innings", NullValueHandling = NullValueHandling.Ignore)]
        public List<InningElement> Innings { get; set; }
    }

    public partial class InningElement
    {
        [JsonProperty("team", NullValueHandling = NullValueHandling.Ignore)]
        public TeamInfoClass Team { get; set; }

        [JsonProperty("inningNumber", NullValueHandling = NullValueHandling.Ignore)]
        public long? InningNumber { get; set; }

        [JsonProperty("inningPartnerships", NullValueHandling = NullValueHandling.Ignore)]
        public List<object> InningPartnerships { get; set; }

        [JsonProperty("inningOvers", NullValueHandling = NullValueHandling.Ignore)]
        public List<object> InningOvers { get; set; }

        [JsonProperty("inningWickets", NullValueHandling = NullValueHandling.Ignore)]
        public List<Inning> InningWickets { get; set; }

        [JsonProperty("inningFallOfWickets", NullValueHandling = NullValueHandling.Ignore)]
        public List<InningFallOfWicket> InningFallOfWickets { get; set; }
    }
    public partial class Inning
    {
        [JsonProperty("playerRoleType", NullValueHandling = NullValueHandling.Ignore)]
        public string PlayerRoleType { get; set; }

        [JsonProperty("player", NullValueHandling = NullValueHandling.Ignore)]
        public DismissalBatsmanClass Player { get; set; }

        [JsonProperty("battedType", NullValueHandling = NullValueHandling.Ignore)]
        public string BattedType { get; set; }

        [JsonProperty("fours")]
        public long? Fours { get; set; }

        [JsonProperty("sixes")]
        public long? Sixes { get; set; }

        [JsonProperty("strikerate")]
        public double? Strikerate { get; set; }

        [JsonProperty("isOut", NullValueHandling = NullValueHandling.Ignore)]
        public bool? IsOut { get; set; }

        [JsonProperty("dismissalType")]
        public long? DismissalType { get; set; }

        [JsonProperty("dismissalBatsman")]
        public DismissalBatsmanClass DismissalBatsman { get; set; }

        [JsonProperty("dismissalBowler")]
        public DismissalBatsmanClass DismissalBowler { get; set; }

        [JsonProperty("dismissalFielders")]
        public List<DismissalFielder> DismissalFielders { get; set; }

        [JsonProperty("dismissalText")]
        public DismissalText DismissalText { get; set; }

        [JsonProperty("dismissalComment")]
        public object DismissalComment { get; set; }

        [JsonProperty("fowOrder")]
        public long? FowOrder { get; set; }

        [JsonProperty("fowWicketNum")]
        public long? FowWicketNum { get; set; }

        [JsonProperty("fowRuns")]
        public long? FowRuns { get; set; }

        [JsonProperty("fowBalls")]
        public object FowBalls { get; set; }

        [JsonProperty("fowOvers")]
        public double? FowOvers { get; set; }

        [JsonProperty("fowOverNumber")]
        public long? FowOverNumber { get; set; }

        [JsonProperty("ballOversActual")]
        public double? BallOversActual { get; set; }

        [JsonProperty("ballOversUnique")]
        public double? BallOversUnique { get; set; }

        [JsonProperty("ballTotalRuns")]
        public long? BallTotalRuns { get; set; }

        [JsonProperty("ballBatsmanRuns")]
        public long? BallBatsmanRuns { get; set; }

        [JsonProperty("videos", NullValueHandling = NullValueHandling.Ignore)]
        public List<object> Videos { get; set; }

        [JsonProperty("images", NullValueHandling = NullValueHandling.Ignore)]
        public List<object> Images { get; set; }

        [JsonProperty("currentType")]
        public long? CurrentType { get; set; }
    }

    public partial class DismissalBatsmanClass
    {
        [JsonProperty("id", NullValueHandling = NullValueHandling.Ignore)]
        public long? Id { get; set; }

        [JsonProperty("objectId", NullValueHandling = NullValueHandling.Ignore)]
        public long? ObjectId { get; set; }

        [JsonProperty("name", NullValueHandling = NullValueHandling.Ignore)]
        public string Name { get; set; }

        [JsonProperty("longName", NullValueHandling = NullValueHandling.Ignore)]
        public string LongName { get; set; }

        [JsonProperty("mobileName", NullValueHandling = NullValueHandling.Ignore)]
        public string MobileName { get; set; }

        [JsonProperty("indexName", NullValueHandling = NullValueHandling.Ignore)]
        public string IndexName { get; set; }

        [JsonProperty("slug", NullValueHandling = NullValueHandling.Ignore)]
        public string Slug { get; set; }

        [JsonProperty("battingName", NullValueHandling = NullValueHandling.Ignore)]
        public string BattingName { get; set; }

        [JsonProperty("fieldingName", NullValueHandling = NullValueHandling.Ignore)]
        public string FieldingName { get; set; }

        [JsonProperty("gender", NullValueHandling = NullValueHandling.Ignore)]
        public string Gender { get; set; }

        [JsonProperty("playingRole", NullValueHandling = NullValueHandling.Ignore)]
        public string PlayingRole { get; set; }

        [JsonProperty("battingStyles", NullValueHandling = NullValueHandling.Ignore)]
        public List<string> BattingStyles { get; set; }

        [JsonProperty("bowlingStyles", NullValueHandling = NullValueHandling.Ignore)]
        public List<string> BowlingStyles { get; set; }

        [JsonProperty("longBattingStyles", NullValueHandling = NullValueHandling.Ignore)]
        public List<string> LongBattingStyles { get; set; }

        [JsonProperty("longBowlingStyles", NullValueHandling = NullValueHandling.Ignore)]
        public List<string> LongBowlingStyles { get; set; }

        [JsonProperty("image")]
        public Image Image { get; set; }

        [JsonProperty("countryTeamId", NullValueHandling = NullValueHandling.Ignore)]
        public long? CountryTeamId { get; set; }

        [JsonProperty("playingRoleType", NullValueHandling = NullValueHandling.Ignore)]
        public string PlayingRoleType { get; set; }

        [JsonProperty("playingRoles", NullValueHandling = NullValueHandling.Ignore)]
        public List<string> PlayingRoles { get; set; }

        [JsonProperty("dateOfBirth", NullValueHandling = NullValueHandling.Ignore)]
        public DateOfBirth DateOfBirth { get; set; }

        [JsonProperty("dateOfDeath")]
        public object DateOfDeath { get; set; }
    }
    public partial class LivePerformance
    {
        [JsonProperty("batsmen", NullValueHandling = NullValueHandling.Ignore)]
        public List<LivePerformanceBatsman> Batsmen { get; set; }

        [JsonProperty("bowlers", NullValueHandling = NullValueHandling.Ignore)]
        public List<LivePerformanceBowler> Bowlers { get; set; }
    }

    public partial class LivePerformanceBatsman
    {
        [JsonProperty("_uid", NullValueHandling = NullValueHandling.Ignore)]
        public long? Uid { get; set; }

        [JsonProperty("player", NullValueHandling = NullValueHandling.Ignore)]
        public DismissalBatsmanClass Player { get; set; }

        [JsonProperty("runs", NullValueHandling = NullValueHandling.Ignore)]
        public long? Runs { get; set; }

        [JsonProperty("balls", NullValueHandling = NullValueHandling.Ignore)]
        public long? Balls { get; set; }

        [JsonProperty("fours", NullValueHandling = NullValueHandling.Ignore)]
        public long? Fours { get; set; }

        [JsonProperty("sixes", NullValueHandling = NullValueHandling.Ignore)]
        public long? Sixes { get; set; }

        [JsonProperty("strikerate", NullValueHandling = NullValueHandling.Ignore)]
        public long? Strikerate { get; set; }

        [JsonProperty("control", NullValueHandling = NullValueHandling.Ignore)]
        public long? Control { get; set; }

        [JsonProperty("shot", NullValueHandling = NullValueHandling.Ignore)]
        public string Shot { get; set; }

        [JsonProperty("shotRuns", NullValueHandling = NullValueHandling.Ignore)]
        public long? ShotRuns { get; set; }

        [JsonProperty("shotFours", NullValueHandling = NullValueHandling.Ignore)]
        public long? ShotFours { get; set; }

        [JsonProperty("shotSixes", NullValueHandling = NullValueHandling.Ignore)]
        public long? ShotSixes { get; set; }

        [JsonProperty("wagonData", NullValueHandling = NullValueHandling.Ignore)]
        public List<long> WagonData { get; set; }

        [JsonProperty("teamAbbreviation", NullValueHandling = NullValueHandling.Ignore)]
        public string TeamAbbreviation { get; set; }
    }

    public partial class LivePerformanceBowler
    {
        [JsonProperty("_uid", NullValueHandling = NullValueHandling.Ignore)]
        public long? Uid { get; set; }

        [JsonProperty("player", NullValueHandling = NullValueHandling.Ignore)]
        public DismissalBatsmanClass Player { get; set; }

        [JsonProperty("lhbPitchMap", NullValueHandling = NullValueHandling.Ignore)]
        public List<HbPitchMap> LhbPitchMap { get; set; }

        [JsonProperty("rhbPitchMap", NullValueHandling = NullValueHandling.Ignore)]
        public List<HbPitchMap> RhbPitchMap { get; set; }

        [JsonProperty("overs", NullValueHandling = NullValueHandling.Ignore)]
        public double? Overs { get; set; }

        [JsonProperty("balls", NullValueHandling = NullValueHandling.Ignore)]
        public long? Balls { get; set; }

        [JsonProperty("dots", NullValueHandling = NullValueHandling.Ignore)]
        public long? Dots { get; set; }

        [JsonProperty("maidens", NullValueHandling = NullValueHandling.Ignore)]
        public long? Maidens { get; set; }

        [JsonProperty("conceded", NullValueHandling = NullValueHandling.Ignore)]
        public long? Conceded { get; set; }

        [JsonProperty("wickets", NullValueHandling = NullValueHandling.Ignore)]
        public long? Wickets { get; set; }

        [JsonProperty("economy", NullValueHandling = NullValueHandling.Ignore)]
        public double? Economy { get; set; }

        [JsonProperty("teamAbbreviation", NullValueHandling = NullValueHandling.Ignore)]
        public string TeamAbbreviation { get; set; }

        [JsonProperty("currentType", NullValueHandling = NullValueHandling.Ignore)]
        public long? CurrentType { get; set; }
    }

    public partial class HbPitchMap
    {
        [JsonProperty("line")]
        public object Line { get; set; }

        [JsonProperty("length")]
        public object Length { get; set; }

        [JsonProperty("runs", NullValueHandling = NullValueHandling.Ignore)]
        public long? Runs { get; set; }

        [JsonProperty("balls", NullValueHandling = NullValueHandling.Ignore)]
        public long? Balls { get; set; }

        [JsonProperty("wickets", NullValueHandling = NullValueHandling.Ignore)]
        public long? Wickets { get; set; }
    }

    public partial class RecentBallCommentary
    {
        [JsonProperty("ballComments", NullValueHandling = NullValueHandling.Ignore)]
        public List<BallComment> BallComments { get; set; }

        [JsonProperty("superOverBallComments", NullValueHandling = NullValueHandling.Ignore)]
        public List<object> SuperOverBallComments { get; set; }

        [JsonProperty("preComments", NullValueHandling = NullValueHandling.Ignore)]
        public List<object> PreComments { get; set; }
    }

    public partial class BallComment
    {
        [JsonProperty("_uid", NullValueHandling = NullValueHandling.Ignore)]
        public long? Uid { get; set; }

        [JsonProperty("id", NullValueHandling = NullValueHandling.Ignore)]
        public long? Id { get; set; }

        [JsonProperty("inningNumber", NullValueHandling = NullValueHandling.Ignore)]
        public long? InningNumber { get; set; }

        [JsonProperty("ballsActual")]
        public object BallsActual { get; set; }

        [JsonProperty("ballsUnique")]
        public object BallsUnique { get; set; }

        [JsonProperty("oversUnique", NullValueHandling = NullValueHandling.Ignore)]
        public double? OversUnique { get; set; }

        [JsonProperty("oversActual", NullValueHandling = NullValueHandling.Ignore)]
        public double? OversActual { get; set; }

        [JsonProperty("overNumber", NullValueHandling = NullValueHandling.Ignore)]
        public long? OverNumber { get; set; }

        [JsonProperty("ballNumber", NullValueHandling = NullValueHandling.Ignore)]
        public long? BallNumber { get; set; }

        [JsonProperty("totalRuns", NullValueHandling = NullValueHandling.Ignore)]
        public long? TotalRuns { get; set; }

        [JsonProperty("batsmanRuns", NullValueHandling = NullValueHandling.Ignore)]
        public long? BatsmanRuns { get; set; }

        [JsonProperty("isFour", NullValueHandling = NullValueHandling.Ignore)]
        public bool? IsFour { get; set; }

        [JsonProperty("isSix", NullValueHandling = NullValueHandling.Ignore)]
        public bool? IsSix { get; set; }

        [JsonProperty("isWicket", NullValueHandling = NullValueHandling.Ignore)]
        public bool? IsWicket { get; set; }

        [JsonProperty("dismissalType")]
        public long? DismissalType { get; set; }

        [JsonProperty("byes", NullValueHandling = NullValueHandling.Ignore)]
        public long? Byes { get; set; }

        [JsonProperty("legbyes", NullValueHandling = NullValueHandling.Ignore)]
        public long? Legbyes { get; set; }

        [JsonProperty("wides", NullValueHandling = NullValueHandling.Ignore)]
        public long? Wides { get; set; }

        [JsonProperty("noballs", NullValueHandling = NullValueHandling.Ignore)]
        public long? Noballs { get; set; }

        [JsonProperty("timestamp")]
        public object Timestamp { get; set; }

        [JsonProperty("batsmanPlayerId", NullValueHandling = NullValueHandling.Ignore)]
        public long? BatsmanPlayerId { get; set; }

        [JsonProperty("bowlerPlayerId", NullValueHandling = NullValueHandling.Ignore)]
        public long? BowlerPlayerId { get; set; }

        [JsonProperty("totalInningRuns", NullValueHandling = NullValueHandling.Ignore)]
        public long? TotalInningRuns { get; set; }

        [JsonProperty("title", NullValueHandling = NullValueHandling.Ignore)]
        public string Title { get; set; }

        [JsonProperty("dismissalText")]
        public DismissalText DismissalText { get; set; }

        [JsonProperty("commentPreTextItems")]
        public object CommentPreTextItems { get; set; }

        [JsonProperty("commentTextItems")]
        public object CommentTextItems { get; set; }

        [JsonProperty("commentPostTextItems")]
        public object CommentPostTextItems { get; set; }

        [JsonProperty("commentVideos", NullValueHandling = NullValueHandling.Ignore)]
        public List<object> CommentVideos { get; set; }

        [JsonProperty("commentImages", NullValueHandling = NullValueHandling.Ignore)]
        public List<object> CommentImages { get; set; }

        [JsonProperty("events", NullValueHandling = NullValueHandling.Ignore)]
        public List<object> Events { get; set; }

        [JsonProperty("over")]
        public Over Over { get; set; }
    }

    public partial class Over
    {
        [JsonProperty("team", NullValueHandling = NullValueHandling.Ignore)]
        public TeamInfoClass Team { get; set; }

        [JsonProperty("overNumber", NullValueHandling = NullValueHandling.Ignore)]
        public long? OverNumber { get; set; }

        [JsonProperty("overRuns", NullValueHandling = NullValueHandling.Ignore)]
        public long? OverRuns { get; set; }

        [JsonProperty("overWickets", NullValueHandling = NullValueHandling.Ignore)]
        public long? OverWickets { get; set; }

        [JsonProperty("totalRuns", NullValueHandling = NullValueHandling.Ignore)]
        public long? TotalRuns { get; set; }

        [JsonProperty("totalWickets", NullValueHandling = NullValueHandling.Ignore)]
        public long? TotalWickets { get; set; }

        [JsonProperty("target", NullValueHandling = NullValueHandling.Ignore)]
        public long? Target { get; set; }

        [JsonProperty("overLimit", NullValueHandling = NullValueHandling.Ignore)]
        public long? OverLimit { get; set; }

        [JsonProperty("ballLimit", NullValueHandling = NullValueHandling.Ignore)]
        public long? BallLimit { get; set; }

        [JsonProperty("isMaiden", NullValueHandling = NullValueHandling.Ignore)]
        public bool? IsMaiden { get; set; }

        [JsonProperty("isComplete", NullValueHandling = NullValueHandling.Ignore)]
        public bool? IsComplete { get; set; }

        [JsonProperty("isSuperOver", NullValueHandling = NullValueHandling.Ignore)]
        public bool? IsSuperOver { get; set; }

        [JsonProperty("overEndBatsmen", NullValueHandling = NullValueHandling.Ignore)]
        public List<OverEndBatsman> OverEndBatsmen { get; set; }

        [JsonProperty("overEndBowlers", NullValueHandling = NullValueHandling.Ignore)]
        public List<OverEndBowler> OverEndBowlers { get; set; }

        [JsonProperty("rollover")]
        public object Rollover { get; set; }
    }

    public partial class OverEndBatsman
    {
        [JsonProperty("player", NullValueHandling = NullValueHandling.Ignore)]
        public DismissalBatsmanClass Player { get; set; }

        [JsonProperty("runs", NullValueHandling = NullValueHandling.Ignore)]
        public long? Runs { get; set; }

        [JsonProperty("balls", NullValueHandling = NullValueHandling.Ignore)]
        public long? Balls { get; set; }
    }

    public partial class OverEndBowler
    {
        [JsonProperty("player", NullValueHandling = NullValueHandling.Ignore)]
        public DismissalBatsmanClass Player { get; set; }

        [JsonProperty("overs", NullValueHandling = NullValueHandling.Ignore)]
        public long? Overs { get; set; }

        [JsonProperty("balls", NullValueHandling = NullValueHandling.Ignore)]
        public long? Balls { get; set; }

        [JsonProperty("maidens", NullValueHandling = NullValueHandling.Ignore)]
        public long? Maidens { get; set; }

        [JsonProperty("conceded", NullValueHandling = NullValueHandling.Ignore)]
        public long? Conceded { get; set; }

        [JsonProperty("wickets", NullValueHandling = NullValueHandling.Ignore)]
        public long? Wickets { get; set; }

        [JsonProperty("currentType", NullValueHandling = NullValueHandling.Ignore)]
        public long? CurrentType { get; set; }
    }

    public partial class Bet365Odds
    {
        [JsonProperty("nextBatsmen", NullValueHandling = NullValueHandling.Ignore)]
        public List<object> NextBatsmen { get; set; }

        [JsonProperty("dismissals", NullValueHandling = NullValueHandling.Ignore)]
        public List<Dismissal> Dismissals { get; set; }
    }

    public partial class Dismissal
    {
        [JsonProperty("type", NullValueHandling = NullValueHandling.Ignore)]
        public string Type { get; set; }

        [JsonProperty("name", NullValueHandling = NullValueHandling.Ignore)]
        public string Name { get; set; }

        [JsonProperty("odds", NullValueHandling = NullValueHandling.Ignore)]
        public string Odds { get; set; }

        [JsonProperty("url", NullValueHandling = NullValueHandling.Ignore)]
        public Uri Url { get; set; }
    }

    public partial class SupportInfoInning
    {
        [JsonProperty("inningNumber", NullValueHandling = NullValueHandling.Ignore)]
        public long? InningNumber { get; set; }

        [JsonProperty("team", NullValueHandling = NullValueHandling.Ignore)]
        public TeamInfoClass Team { get; set; }

        [JsonProperty("isBatted", NullValueHandling = NullValueHandling.Ignore)]
        public bool? IsBatted { get; set; }

        [JsonProperty("runs", NullValueHandling = NullValueHandling.Ignore)]
        public long? Runs { get; set; }

        [JsonProperty("wickets", NullValueHandling = NullValueHandling.Ignore)]
        public long? Wickets { get; set; }

        [JsonProperty("lead", NullValueHandling = NullValueHandling.Ignore)]
        public long? Lead { get; set; }

        [JsonProperty("target", NullValueHandling = NullValueHandling.Ignore)]
        public long? Target { get; set; }

        [JsonProperty("overs", NullValueHandling = NullValueHandling.Ignore)]
        public double? Overs { get; set; }

        [JsonProperty("balls", NullValueHandling = NullValueHandling.Ignore)]
        public long? Balls { get; set; }

        [JsonProperty("totalOvers", NullValueHandling = NullValueHandling.Ignore)]
        public long? TotalOvers { get; set; }

        [JsonProperty("totalBalls", NullValueHandling = NullValueHandling.Ignore)]
        public long? TotalBalls { get; set; }

        [JsonProperty("minutes")]
        public object Minutes { get; set; }

        [JsonProperty("extras", NullValueHandling = NullValueHandling.Ignore)]
        public long? Extras { get; set; }

        [JsonProperty("byes", NullValueHandling = NullValueHandling.Ignore)]
        public long? Byes { get; set; }

        [JsonProperty("legbyes", NullValueHandling = NullValueHandling.Ignore)]
        public long? Legbyes { get; set; }

        [JsonProperty("wides", NullValueHandling = NullValueHandling.Ignore)]
        public long? Wides { get; set; }

        [JsonProperty("noballs", NullValueHandling = NullValueHandling.Ignore)]
        public long? Noballs { get; set; }

        [JsonProperty("penalties", NullValueHandling = NullValueHandling.Ignore)]
        public long? Penalties { get; set; }

        [JsonProperty("event")]
        public object Event { get; set; }

        [JsonProperty("ballsPerOver", NullValueHandling = NullValueHandling.Ignore)]
        public long? BallsPerOver { get; set; }

        [JsonProperty("inningBatsmen", NullValueHandling = NullValueHandling.Ignore)]
        public List<Inning> InningBatsmen { get; set; }

        [JsonProperty("inningBowlers", NullValueHandling = NullValueHandling.Ignore)]
        public List<InningBowler> InningBowlers { get; set; }

        [JsonProperty("inningPartnerships", NullValueHandling = NullValueHandling.Ignore)]
        public List<object> InningPartnerships { get; set; }

        [JsonProperty("inningOvers", NullValueHandling = NullValueHandling.Ignore)]
        public List<object> InningOvers { get; set; }

        [JsonProperty("inningWickets", NullValueHandling = NullValueHandling.Ignore)]
        public List<Inning> InningWickets { get; set; }

        [JsonProperty("inningFallOfWickets", NullValueHandling = NullValueHandling.Ignore)]
        public List<InningFallOfWicket> InningFallOfWickets { get; set; }
    }
    public partial class LiveInfo
    {
        [JsonProperty("type", NullValueHandling = NullValueHandling.Ignore)]
        public string Type { get; set; }

        [JsonProperty("currentRunRate", NullValueHandling = NullValueHandling.Ignore)]
        public double? CurrentRunRate { get; set; }

        [JsonProperty("requiredRunrate", NullValueHandling = NullValueHandling.Ignore)]
        public long? RequiredRunrate { get; set; }

        [JsonProperty("lastFewOversRuns", NullValueHandling = NullValueHandling.Ignore)]
        public long? LastFewOversRuns { get; set; }

        [JsonProperty("lastFewOversWickets", NullValueHandling = NullValueHandling.Ignore)]
        public long? LastFewOversWickets { get; set; }

        [JsonProperty("lastFewOversRunrate", NullValueHandling = NullValueHandling.Ignore)]
        public double? LastFewOversRunrate { get; set; }
    }

    public partial class LiveSummary
    {
        [JsonProperty("recentBalls", NullValueHandling = NullValueHandling.Ignore)]
        public List<RecentBall> RecentBalls { get; set; }

        [JsonProperty("batsmen", NullValueHandling = NullValueHandling.Ignore)]
        public List<LiveSummaryBatsman> Batsmen { get; set; }

        [JsonProperty("bowlers", NullValueHandling = NullValueHandling.Ignore)]
        public List<LiveSummaryBowler> Bowlers { get; set; }

        [JsonProperty("partnershipText", NullValueHandling = NullValueHandling.Ignore)]
        public string PartnershipText { get; set; }

        [JsonProperty("lastBatText", NullValueHandling = NullValueHandling.Ignore)]
        public string LastBatText { get; set; }

        [JsonProperty("reviewsText", NullValueHandling = NullValueHandling.Ignore)]
        public string ReviewsText { get; set; }

        [JsonProperty("reviewsShortText", NullValueHandling = NullValueHandling.Ignore)]
        public string ReviewsShortText { get; set; }

        [JsonProperty("fowText", NullValueHandling = NullValueHandling.Ignore)]
        public string FowText { get; set; }
    }

    public partial class LiveSummaryBatsman
    {
        [JsonProperty("_uid", NullValueHandling = NullValueHandling.Ignore)]
        public long? Uid { get; set; }

        [JsonProperty("player", NullValueHandling = NullValueHandling.Ignore)]
        public DismissalBatsmanClass Player { get; set; }

        [JsonProperty("runs", NullValueHandling = NullValueHandling.Ignore)]
        public long? Runs { get; set; }

        [JsonProperty("balls", NullValueHandling = NullValueHandling.Ignore)]
        public long? Balls { get; set; }

        [JsonProperty("fours", NullValueHandling = NullValueHandling.Ignore)]
        public long? Fours { get; set; }

        [JsonProperty("sixes", NullValueHandling = NullValueHandling.Ignore)]
        public long? Sixes { get; set; }

        [JsonProperty("strikerate", NullValueHandling = NullValueHandling.Ignore)]
        public long? Strikerate { get; set; }

        [JsonProperty("battedType", NullValueHandling = NullValueHandling.Ignore)]
        public string BattedType { get; set; }

        [JsonProperty("currentBowlerRuns", NullValueHandling = NullValueHandling.Ignore)]
        public long? CurrentBowlerRuns { get; set; }

        [JsonProperty("currentBowlerBalls", NullValueHandling = NullValueHandling.Ignore)]
        public long? CurrentBowlerBalls { get; set; }

        [JsonProperty("lastFewOversRuns", NullValueHandling = NullValueHandling.Ignore)]
        public long? LastFewOversRuns { get; set; }

        [JsonProperty("lastFewOversBalls", NullValueHandling = NullValueHandling.Ignore)]
        public long? LastFewOversBalls { get; set; }

        [JsonProperty("careerMatches", NullValueHandling = NullValueHandling.Ignore)]
        public long? CareerMatches { get; set; }

        [JsonProperty("careerRuns", NullValueHandling = NullValueHandling.Ignore)]
        public long? CareerRuns { get; set; }

        [JsonProperty("careerAverage", NullValueHandling = NullValueHandling.Ignore)]
        public double? CareerAverage { get; set; }

        [JsonProperty("careerHS", NullValueHandling = NullValueHandling.Ignore)]
        public long? CareerHs { get; set; }

        [JsonProperty("careerHSNotOut", NullValueHandling = NullValueHandling.Ignore)]
        public bool? CareerHsNotOut { get; set; }

        [JsonProperty("videos", NullValueHandling = NullValueHandling.Ignore)]
        public List<object> Videos { get; set; }

        [JsonProperty("images", NullValueHandling = NullValueHandling.Ignore)]
        public List<object> Images { get; set; }

        [JsonProperty("currentType", NullValueHandling = NullValueHandling.Ignore)]
        public long? CurrentType { get; set; }
    }

    public partial class LiveSummaryBowler
    {
        [JsonProperty("_uid", NullValueHandling = NullValueHandling.Ignore)]
        public long? Uid { get; set; }

        [JsonProperty("player", NullValueHandling = NullValueHandling.Ignore)]
        public DismissalBatsmanClass Player { get; set; }

        [JsonProperty("overs", NullValueHandling = NullValueHandling.Ignore)]
        public double? Overs { get; set; }

        [JsonProperty("balls", NullValueHandling = NullValueHandling.Ignore)]
        public long? Balls { get; set; }

        [JsonProperty("maidens", NullValueHandling = NullValueHandling.Ignore)]
        public long? Maidens { get; set; }

        [JsonProperty("conceded", NullValueHandling = NullValueHandling.Ignore)]
        public long? Conceded { get; set; }

        [JsonProperty("wickets", NullValueHandling = NullValueHandling.Ignore)]
        public long? Wickets { get; set; }

        [JsonProperty("economy", NullValueHandling = NullValueHandling.Ignore)]
        public double? Economy { get; set; }

        [JsonProperty("dots", NullValueHandling = NullValueHandling.Ignore)]
        public long? Dots { get; set; }

        [JsonProperty("fours", NullValueHandling = NullValueHandling.Ignore)]
        public long? Fours { get; set; }

        [JsonProperty("sixes", NullValueHandling = NullValueHandling.Ignore)]
        public long? Sixes { get; set; }

        [JsonProperty("spellBalls", NullValueHandling = NullValueHandling.Ignore)]
        public long? SpellBalls { get; set; }

        [JsonProperty("spellOvers", NullValueHandling = NullValueHandling.Ignore)]
        public long? SpellOvers { get; set; }

        [JsonProperty("spellMaidens", NullValueHandling = NullValueHandling.Ignore)]
        public long? SpellMaidens { get; set; }

        [JsonProperty("spellRuns", NullValueHandling = NullValueHandling.Ignore)]
        public long? SpellRuns { get; set; }

        [JsonProperty("spellWickets", NullValueHandling = NullValueHandling.Ignore)]
        public long? SpellWickets { get; set; }

        [JsonProperty("careerMatches", NullValueHandling = NullValueHandling.Ignore)]
        public long? CareerMatches { get; set; }

        [JsonProperty("careerWickets", NullValueHandling = NullValueHandling.Ignore)]
        public long? CareerWickets { get; set; }

        [JsonProperty("careerAverage", NullValueHandling = NullValueHandling.Ignore)]
        public double? CareerAverage { get; set; }

        [JsonProperty("careerBBIWickets", NullValueHandling = NullValueHandling.Ignore)]
        public long? CareerBbiWickets { get; set; }

        [JsonProperty("careerBBIRuns", NullValueHandling = NullValueHandling.Ignore)]
        public long? CareerBbiRuns { get; set; }

        [JsonProperty("videos", NullValueHandling = NullValueHandling.Ignore)]
        public List<object> Videos { get; set; }

        [JsonProperty("images", NullValueHandling = NullValueHandling.Ignore)]
        public List<object> Images { get; set; }

        [JsonProperty("currentType", NullValueHandling = NullValueHandling.Ignore)]
        public long? CurrentType { get; set; }

        [JsonProperty("rollOver", NullValueHandling = NullValueHandling.Ignore)]
        public bool? RollOver { get; set; }
    }

    public partial class RecentBall
    {
        [JsonProperty("_uid", NullValueHandling = NullValueHandling.Ignore)]
        public long? Uid { get; set; }

        [JsonProperty("id", NullValueHandling = NullValueHandling.Ignore)]
        public long? Id { get; set; }

        [JsonProperty("inningNumber", NullValueHandling = NullValueHandling.Ignore)]
        public long? InningNumber { get; set; }

        [JsonProperty("ballsActual")]
        public object BallsActual { get; set; }

        [JsonProperty("ballsUnique")]
        public object BallsUnique { get; set; }

        [JsonProperty("oversUnique", NullValueHandling = NullValueHandling.Ignore)]
        public double? OversUnique { get; set; }

        [JsonProperty("oversActual", NullValueHandling = NullValueHandling.Ignore)]
        public double? OversActual { get; set; }

        [JsonProperty("overNumber", NullValueHandling = NullValueHandling.Ignore)]
        public long? OverNumber { get; set; }

        [JsonProperty("ballNumber", NullValueHandling = NullValueHandling.Ignore)]
        public long? BallNumber { get; set; }

        [JsonProperty("totalRuns", NullValueHandling = NullValueHandling.Ignore)]
        public long? TotalRuns { get; set; }

        [JsonProperty("batsmanRuns", NullValueHandling = NullValueHandling.Ignore)]
        public long? BatsmanRuns { get; set; }

        [JsonProperty("isFour", NullValueHandling = NullValueHandling.Ignore)]
        public bool? IsFour { get; set; }

        [JsonProperty("isSix", NullValueHandling = NullValueHandling.Ignore)]
        public bool? IsSix { get; set; }

        [JsonProperty("isWicket", NullValueHandling = NullValueHandling.Ignore)]
        public bool? IsWicket { get; set; }

        [JsonProperty("dismissalType")]
        public long? DismissalType { get; set; }

        [JsonProperty("byes", NullValueHandling = NullValueHandling.Ignore)]
        public long? Byes { get; set; }

        [JsonProperty("legbyes", NullValueHandling = NullValueHandling.Ignore)]
        public long? Legbyes { get; set; }

        [JsonProperty("wides", NullValueHandling = NullValueHandling.Ignore)]
        public long? Wides { get; set; }

        [JsonProperty("noballs", NullValueHandling = NullValueHandling.Ignore)]
        public long? Noballs { get; set; }

        [JsonProperty("timestamp")]
        public object Timestamp { get; set; }

        [JsonProperty("batsmanPlayerId", NullValueHandling = NullValueHandling.Ignore)]
        public long? BatsmanPlayerId { get; set; }

        [JsonProperty("bowlerPlayerId", NullValueHandling = NullValueHandling.Ignore)]
        public long? BowlerPlayerId { get; set; }

        [JsonProperty("totalInningRuns", NullValueHandling = NullValueHandling.Ignore)]
        public long? TotalInningRuns { get; set; }
    }

    public partial class Group
    {
        [JsonProperty("name", NullValueHandling = NullValueHandling.Ignore)]
        public string Name { get; set; }

        [JsonProperty("teamStats", NullValueHandling = NullValueHandling.Ignore)]
        public List<TeamStat> TeamStats { get; set; }
    }

}