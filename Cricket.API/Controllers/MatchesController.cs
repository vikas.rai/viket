using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Cricket.Business.Matches;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Cricket.API.Controllers
{
    [EnableCors("AllowOrigin")]
    [Route("api/[controller]")]
    [ApiController]
    public class MatchesController : ControllerBase
    {
        public MatchesController()
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
        }
        [HttpGet("Current")]
        public async Task<IActionResult> Current()
        {
            Matches m = new Matches();
            return Ok(await m.GetCurrent());
        }

        [HttpGet("Trending")]
        public async Task<IActionResult> Trending()
        {
            Matches m = new Matches();
            return Ok(await m.GetTrending());
        }

        [HttpGet("Scheduled")]
        public async Task<IActionResult> Scheduled()
        {
            Matches m = new Matches();
            return Ok(await m.GetScheduled());
        }

        [HttpGet("Details")]
        public async Task<IActionResult> Details(long SeriesID, long MatchID)
        {
            Matches m = new Matches();

            return Ok(await m.Details(SeriesID, MatchID));
        }

        [HttpGet("ScoreCard")]
        public async Task<IActionResult> ScoreCard(long SeriesID, long MatchID)
        {
            Matches m = new Matches();

            return Ok(await m.ScoreCard(SeriesID, MatchID));
        }


        [HttpGet("LiveMatch")]
        public async Task<IActionResult> LiveMatch(long SeriesID, long MatchID)
        {
            Matches m = new Matches();

            return Ok(await m.GetLiveMatch(SeriesID, MatchID));
        }

        [HttpGet("Preview")]
        public async Task<IActionResult> Preview(long SeriesID, long MatchID)
        {
            Matches m = new Matches();
            var prview = await m.Preview(SeriesID, MatchID);
            var Squad = await m.Squad(SeriesID, MatchID);
            return Ok(new { prview, Squad });
        }
        [HttpGet("PlayingXI")]
        public async Task<IActionResult> PlayingXI(long SeriesID, long MatchID)
        {
            Matches m = new Matches();
            var data = await m.PlayingXI(SeriesID, MatchID);
            return Ok(new { PlayingXI = data.Item1, Bench = data.Item2 });
        }

        [HttpGet("Fantasy")]
        public async Task<IActionResult> Fantasy(long SeriesID, long MatchID)
        {
            Matches m = new Matches();
            var Fantasy = await m.Fantasy(SeriesID, MatchID);
            var Squad = await m.Squad(SeriesID, MatchID);
            return Ok(new { Fantasy, Squad });

        }
    }
}